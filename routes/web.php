<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
});*/



Route::group(['middleware' => ['auth']], function () {
	
	Route::get('/', 'HomeController@loadCustomers');
	Route::get('/home','HomeController@loadCustomers');
	Route::get('/dashboard', 'HomeController@loadCustomers');
	Route::get('/orders', 'HomeController@loadOrders');
	Route::get('/products', 'HomeController@loadProducts');
	Route::get('/search-customers', 'CustomerController@search');
	Route::get('/import', function(){
		return view("dashboard.import");
	});

	
	Route::get('/order/refund/{id}', 'OrderController@refunds');
	Route::get('/order/newOrder/{id}', 'OrderController@createOrder');


	Route::post('/import', 'HomeController@csvImport');
	Route::post('/order-details', 'OrderController@addOrderDetails');
	Route::post('/order/refund/{id}', 'OrderController@braintreeRefund');
	Route::post('/order/void/{id}', 'OrderController@braintreeVoid');
	


	Route::resource('customer', 'CustomerController');
	Route::resource('order', 'OrderController');

	Route::get('cronjob/import', 'HomeController@importJob');

	//temporary routes
	/*Route::get('/demoOrder', 'HomeController@demoOrder');
	Route::get('/demoCustomer', 'HomeController@demoCustomer');
	Route::get('/shippingmethods', 'HomeController@shippingMethodsDemo');*/

});

/* Route::get('posts/create', 'PostsController@create')
        ->name('posts.create')
        ->middleware('auth'); */

Auth::routes();


