$(document).ready(function(){


	$(".orderdetails-row .quantity input, .orderdetails-row .discount input").change(function(){
		updateOrderDetailsTotal($(this));
	})

	$(" .order-tax input, .order-discount input").change(function(){
		updateOrderDetailsTotal($(this));
	})

	
});

function updateOrderDetailsTotal(ele)
{
	var row = ele.parents("tr");
	var quantity = row.find(".quantity input").val(); quantity = (!$.isNumeric(quantity) || quantity.trim() == "") ? "0":quantity;
	var price = row.find(".unitprice input").val(); price = (!$.isNumeric(price) || price.trim() == "") ? "0":price;
	var discount = row.find(".discount input").val(); discount = (!$.isNumeric(discount) || discount.trim() == "") ? "0":discount;

	var total = parseFloat(quantity)*parseFloat(price)*parseFloat(1-parseFloat(discount)/100);
	row.find(".unittotal input").val(total.toFixed(2));

	//update Order subtotal
	
	calculateTotal();
	checkShipping();
}


function checkShipping()
{
	site = $("#website_id :selected").text();
	if(site == undefined || site_shipping_type == undefined || site_shipping_type[site] == undefined)
		return;

	switch(site_shipping_type[site]) {
	    case "delivery_location":
	    	var local = false;
	        if($("#show-shipping").is(":checked")) //get shipping country
	        {
	        	if($("input[name='shipping_country']").val().trim().toUpperCase() == "US")
	        		local = true;
	        }
	        else
	        {
	        	//get billing country
	        	if($("#customer-billing-country").val() != undefined && $("#customer-billing-country").val().trim().toUpperCase() == "US")
	        		local = true;
	        }

	        if(!local)
	        	$(".order-shipping input[name='shipping_cost']").val("20.00");
	        else
	        	$(".order-shipping input[name='shipping_cost']").val("0.00");
	       

	        break;
	    case "order_total":
	        if(parseFloat($(".order-subtotal input").val()) >= 125)
	        	$(".order-shipping input[name='shipping_cost']").val("0.00");
	        else
	        	$(".order-shipping input[name='shipping_cost']").val("6.00");
	        break;
	    case "products_weight":
	        //get weights

	        var weights = 0;

	        $(".sku-field").each(function(){

	        	var element = $(this).parents("tr");
				var id = $(this).val();
				var text = $(this).children(':selected').text();
				var quantity = parseFloat($(element).find('.quantity input').val());
	        	var lookupRow = $("#products-lookup div[data-sku='"+text+"']");

				weights += parseFloat(lookupRow.find(".weight").text())*quantity;
	        })

	        console.log("weights = "+weights);

	        if(weights != undefined && weights < 390)
	        {
	        	if(weights <= 120)
	        	{
	        		$(".order-shipping input[name='shipping_cost']").val("2.99");
	        	}
	        	else
	        	{
	        		$(".order-shipping input[name='shipping_cost']").val("3.74");
	        	}
	        }	
	        else
	        {
	        	//calculate by subtotal
	        	var subtotal = parseFloat($(".order-subtotal input").val());

	        	if(subtotal < 100)
	        		$(".order-shipping input[name='shipping_cost']").val("7.45");
	        	else if(subtotal < 200 && subtotal >= 100)
	        		$(".order-shipping input[name='shipping_cost']").val("11.45");
	        	else if(subtotal < 400 && subtotal >= 200)
	        		$(".order-shipping input[name='shipping_cost']").val("14.95");
	        	else if(subtotal < 500 && subtotal >= 400)
	        		$(".order-shipping input[name='shipping_cost']").val("17.95");
	        	else if(subtotal >= 500)
	        		$(".order-shipping input[name='shipping_cost']").val("19.99");
	        	else
	        		$(".order-shipping input[name='shipping_cost']").val("0.00");
	        }
	        break;
	    default:
	    	$(".order-shipping input[name='shipping_cost']").val("0.00");
	        break;
	}

	console.log("update shipping with type: "+site_shipping_type[site]);
}

function calculateTotal()
{
	var total = 0;
	$(".orderdetails-row").each(function(){
		var row = $(this);
		var quantity = row.find(".quantity input").val(); quantity = (!$.isNumeric(quantity) || quantity.trim() == "") ? "0":quantity;
		var price = row.find(".unitprice input").val(); price = (!$.isNumeric(price) || price.trim() == "") ? "0":price;
		var discount = row.find(".discount input").val(); discount = (!$.isNumeric(discount) || discount.trim() == "") ? "0":discount;

		total+= parseFloat(quantity)*parseFloat(price)*parseFloat(1-parseFloat(discount)/100);
	})
	

	//update Order subtotal
	$(".order-subtotal input").val(total.toFixed(2));

	//update Order total
	var shipping = $(".order-shipping input").val(); shipping = (!$.isNumeric(shipping) || shipping.trim() == "") ? "0":shipping;
	var tax = $(".order-tax input").val(); tax = (!$.isNumeric(tax) || tax.trim() == "") ? "0":tax;
	var discount = $(".order-discount input").val(); discount = (!$.isNumeric(discount) || discount.trim() == "") ? "0":discount;

	total+= parseFloat(shipping)+parseFloat(tax)-parseFloat(discount);
	$(".order-total input").val(parseFloat(total).toFixed(2));

	$(".payment-charge input").val(parseFloat(total).toFixed(2));

}


function unique(list) {
  var result = [];
  $.each(list, function(i, e) {
    if ($.inArray(e, result) == -1) result.push(e);
  });
  return result;
}