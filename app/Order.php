<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    protected $visible = ['id', 'customer_id', 'order_id', 'website_id','order_date','status', 'subtotal','discount', 'shipping_cost' , 'tax','total_charged', 'approval_code','credit_card','shipping_first_name','shipping_last_name',
        'shipping_address_1','shipping_address_2','shipping_postcode','shipping_city','shipping_state','shipping_country','shipping_company','created_at', 'updated_at' ];



    public function website()
    {
        return $this->belongsTo('App\Website');
    }

    public function buyer()
    {
        return $this->belongsTo('App\Customer','id');
    }

    public function details()
    {
        return $this->hasMany('App\OrdersDetails');
    }
}