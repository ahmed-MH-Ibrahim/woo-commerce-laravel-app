<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class OrdersDetails extends Model
{
    protected $visible = ['id', 'order_id', 'sku','quantity','unit_price', 'discount','website_id','created_at', 'updated_at' ];



    public function website()
    {
        return $this->belongsTo('App\Website');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}