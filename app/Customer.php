<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class Customer extends Model
{
    protected $visible = ['id', 'customer_id','first_name', 'website_id','last_name','address_1', 'address_2','city','state','zip','country','phone','company','email','comments','created_at', 'updated_at' ];

    protected $fillable = [ 'customer_id','first_name', 'website_id','last_name','address_1', 'address_2','city','state','zip','country','phone','company','email','comments'];

    public function website()
    {
        return $this->belongsTo('App\Website');
    }

    public function orders()
    {
        return $this->hasMany('App\Order',/*'id'*/'customer_id');
    }
}