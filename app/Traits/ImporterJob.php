<?php

namespace App\Traits;
use App\UserReport;
use App\Customer;
use App\Order;
use App\OrdersDetails;
use App\Product;
use App\Website;
use Log;

trait ImporterJob{
	/**
	 * Get all of the product's photos.
	 */

	public function headerCheck($line,$lineCounter,$file,$keys)
	{
		if(!$this->array_keys_exists($keys,$line))
		{
			echo "An error occurred. Please show the following to your supervisor: <br/><br/> Empty/Missing Field, row $lineCounter in file $file <br/><br/>";
			$this->errorFound("Empty/Missing Field in row $lineCounter",$file);
			return false;
		}

		return true;
	}

	function array_keys_exists($keys, $line) {
	   return !array_diff_key(array_flip($keys), $line);
	}

	function errorFound($message,$filename)
    {
        $to = "greghowlett@gmail.com";
        $subject = "Error reporting for file $filename";

        $text= "<br/><strong>File: </strong>".$filename." \r\n <br/><br/>";
        $text.= "<br/><strong>Error: </strong>".$message." \r\n <br/><br/>";


        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 

        //var_dump($message);
       // $mailsent = mail($to,$subject,$text,$headers);
    }

	public function importJob()
	{
		$path    = '../orders/';
		$completedPath = "../completed_imports/";
		$files = scandir($path);

		$files = array_diff(scandir($path), array('.', '..'));
		
		$fileCount = count($files) ;

		//var_dump($files);

		try{

			if ( $fileCount <= 0) {
				echo "No files found. Please make sure files are added and try running the script again.  <br/><br/>";
				echo "Back to <a href='".url('/')."'>Dashboard</a><br/><br/>";
	            return ;//response()->json(["error" => "No files found !"]);
	        }

	         
	        $sitesConfig = config('websites.sites');

	        $sites = Website::all();
            $websites = [];

            foreach ($sites as $row) {
                $websites[$row->short] = $row->id;
            }

            echo "The following files were processed: <br/><br/>";

	        foreach ($files as $i => $file) 
	        {

	        	if(!file_exists($path.$file))
	        		continue;

	        	$lineCounter=0;
	       		$exceptionMsg = "";
	        	//$file = $files[$i];

	        	$short = substr($file, 0, 3);

	        	echo $file."<br/><br/>";
	        	Log::info("Processing File $file");


	            if(!array_key_exists($short,$sitesConfig))
	            {
	            	Log::info("Site Source not recognized");
	            	echo "An error occurred. Please show the following to your supervisor: <br/><br/> Site Source not recognized <br/><br/>";
	                return; //response()->json(["error" => "Site Source not recognized"]); 
	            }

	            //$csv = array_map('str_getcsv', file($path.$file));
	            $csv = fopen($path.$file, 'r');

	            //move file to completed import
	            //copy($path.$file, $completedPath.$file);
	            //unlink($path.$file);

	            

	            $headers = [];
	            $keys = [];

	            while (!feof($csv)) {
	            	$line = fgetcsv($csv);
	            //foreach ($csv as $line) {
	       
	                if($lineCounter++==0)
	                {
	                    //get headers                    
	                    foreach ($line as $key => $value) {
	                        $headers[$value] = $key;
	                        $keys[] = $key;
	                    }

	                    //dd($keys);
	                    continue;
	                }
	                    
	                if($line == false || !$this->headerCheck($line,$lineCounter,$file,$keys))
	                {	              	
	                	continue;
	                }

	                //upsert customer
	                $customer = null;

	                /*if(isset($headers['billing_email']) && $headers['billing_email'] != null 
	                	&& isset($line[$headers['billing_email']]) && $line[$headers['billing_email']] != null)*/
	                	$customer = Customer::where('email', $line[$headers['billing_email']])->first();

	                if($customer == null)
	                {

	                	if(!isset($line[$headers['customer_id']]) || $line[$headers['customer_id']] == null)
	                	{
	                		echo "An error occurred. Please show the following to your supervisor: <br/><br/> Empty customer id field, row $lineCounter in file $file <br/><br/>";
	                		$this->errorFound("Empty customer id field, row $lineCounter",$file);
	                		continue;
	                	}

	                    //create customer
	                    $customer = new Customer();

	                    $customer->customer_id = $line[$headers['customer_id']];
	                    $customer->first_name = $line[$headers['billing_first_name']];
	                    $customer->website_id = $websites[$short];
	                    $customer->last_name = $line[$headers['billing_last_name']];
	                    $customer->address_1 = $line[$headers['billing_address_1']];
	                    $customer->address_2 = $line[$headers['billing_address_2']];
	                    $customer->city = $line[$headers['billing_city']];
	                    $customer->state = $line[$headers['billing_state']];
	                    $customer->zip = $line[$headers['billing_postcode']];
	                    $customer->country = $line[$headers['billing_country']];
	                    $customer->company = $line[$headers['billing_company']];
	                    $customer->phone = $line[$headers['billing_phone']];
	                    $customer->email = $line[$headers['billing_email']];
	                    $customer->comments = $line[$headers['customer_note']];
	                    $customer->save();
	                }

	                //upsert product
	                if(!isset($line[$headers['item_sku']]) || $line[$headers['item_sku']] == null || trim($line[$headers['item_sku']]) == '')
	                {
	                	echo "An error occurred. Please show the following to your supervisor: <br/><br/> Empty SKU field, row $lineCounter in file $file <br/><br/>";
	                    $this->errorFound("Empty SKU field, row $lineCounter",$file);
	                    continue;
	                }

	                $product = Product::where("sku", $line[$headers['item_sku']])->first();


	                if($product == null)
	                {
	                    $product = new Product();
	                    $product->name = $line[$headers['item_name']];
	                    $product->sku = $line[$headers['item_sku']];
	                    $product->unit_price = $line[$headers['item_subtotal']];
	                    $product->weight = $line[$headers['item_weight']];
	                    $product->website_id = $websites[$short];
	                    $product->save();
	                }

	                //upsert order
	                $order = null;

	                
	                //if(isset($line[$headers['order_id']]) && $line[$headers['order_id']] != null)
	                	$order = Order::where("order_id", $line[$headers['order_id']])
	                                ->where("website_id",$websites[$short])
	                                ->first();

	                $newOrder = false;

	                if($order == null)
	                {
	                    $order = new Order();
	                    $order->customer_id = $customer->id;
	                    $order->order_id = $line[$headers['order_id']];
	                    $order->order_date = date('Y-m-d H:i:s', strtotime($line[$headers['date']])); //date('Y-m-d H:i:s');//$line[$headers['date']];
	                    $order->status = $line[$headers['status']];
	                    $order->subtotal = 0;//(float)$line[$headers['item_subtotal']]*(float)$line[$headers['item_quantity']];
	                    $order->discount = $line[$headers['discount_total']];
	                    $order->shipping_cost = $line[$headers['shipping_total']];
	                    $order->tax = $line[$headers['tax_total']];
	                    $order->total_charged = $line[$headers['order_total']];
	                    $order->approval_code = $line[$headers['BraintreeApprovalCode']];
	                    $order->credit_card = '';
	                    $order->website_id = $websites[$short];

	                    $order->shipping_first_name = $line[$headers['shipping_first_name']];
	                    $order->website_id = $websites[$short];
	                    $order->shipping_last_name = $line[$headers['shipping_last_name']];
	                    $order->shipping_address_1 = $line[$headers['shipping_address_1']];
	                    $order->shipping_address_2 = $line[$headers['shipping_address_2']];
	                    $order->shipping_city = $line[$headers['shipping_city']];
	                    $order->shipping_state = $line[$headers['shipping_state']];
	                    $order->shipping_postcode = $line[$headers['shipping_postcode']];
	                    $order->shipping_country = $line[$headers['shipping_country']];
	                    $order->shipping_company = $line[$headers['shipping_company']];

	                    $order->save();

	                    $newOrder = true;
	                }


	                //upsert order_details

	                //Note: The order id I use for order details is the local order's id not the imported one (external site's order id)

	                $orderDetails = OrdersDetails::where("order_id", $order->id)
	                                ->where("website_id",$websites[$short])
	                                ->where("sku",$line[$headers['item_sku']])
	                                ->first();

	                if($orderDetails == null)
	                {
	                    $orderDetails = new OrdersDetails();

	                    $orderDetails->order_id = $order->id;
	                    $orderDetails->sku = $product->sku;
	                    $orderDetails->quantity = $line[$headers['item_quantity']];
	                    $orderDetails->unit_price = $line[$headers['item_subtotal']];
	                    $orderDetails->website_id = $websites[$short];

	                    if((float)$line[$headers['item_subtotal']] == 0)
	                        $orderDetails->discount = 0;
	                    else
	                        $orderDetails->discount = (float)(((float)$line[$headers['discount_total']]/(float)$line[$headers['item_subtotal']])*100);
	                    $orderDetails->save();


	                    //add to order's subtotal
	                    $order->subtotal += (float)$line[$headers['item_subtotal']];
	                    $order->update();
	                    /*$order->total_charged = $order->subtotal-$order->discount+$order->tax+$order->shipping_cost;
	                    ;*/

	                        
	                }   


	            }

	            Log::info("Completed File $file");
	            fclose($csv);

	            copy($path.$file, $completedPath.$file);
	            unlink($path.$file);

	           

	        }

	        echo "Back to <a href='".url('/')."'>Dashboard</a><br/><br/>";
	    } catch (Exception $e) {

            $csvFile = $request->file('file');

            $filename = $csvFile->getClientOriginalName();

            //Send email

            //return error

            return response()->json(["error" => "Exception: ".$e->getMessage()." From file $filename. Orders processed ".($lineCounter-1)]);
        }                
	}
}