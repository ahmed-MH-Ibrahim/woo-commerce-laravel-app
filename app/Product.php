<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    protected $visible = ['id','sku', 'website_id','name','unit_price', 'weight','created_at', 'updated_at' ];



    public function website()
    {
        return $this->belongsTo('App\Website');
    }

    public function orderDetails()
    {
        return $this->hasMany('App\OrderDetails');
    }
}