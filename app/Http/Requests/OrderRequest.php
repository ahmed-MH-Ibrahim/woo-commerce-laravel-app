<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$data = $request->all();

        $data = $this->request->all();
        //dd($data);

        $validatArr = [
            'website_id' => "required",
            'status' => "required",
            'shipping_first_name' => "required_if:show-shipping,==,enabled",
            'shipping_last_name' => "required_if:show-shipping,==,enabled",
            'shipping_address_1' => "required_if:show-shipping,==,enabled",
            'shipping_postcode' => "required_if:show-shipping,==,enabled",
            'shipping_city' => "required_if:show-shipping,==,enabled",
            'shipping_state' => "required_if:show-shipping,==,enabled",
            'shipping_country' => "required_if:show-shipping,==,enabled",
            'shipping_cost' => "required|numeric|min:0",
            'total_discount' => "required|numeric|min:0",
            'tax' => "required|numeric|min:0",
            'credit_card' => "required|numeric",
            'credit_card_cvn' => "required|numeric",
            'credit_card_exp' => "required|numeric|max:1300",//date_format:m/Y",
            'customer-id' => "required|exists:customers,id"
        ];

        

        foreach ($data["sku"] as $key => $value) {
            //echo "$key <br/> $value <br/><br/>";
            $validatArr["sku.$key"] = "required|exists:products,sku";
        }

        foreach ($data["quantity"] as $key => $value) {
            //echo "$key <br/> $value <br/><br/>";
            $validatArr["quantity.$key"] = "required";
        }

        foreach ($data["discount"] as $key => $value) {
            //echo "$key <br/> $value <br/><br/>";
            $validatArr["discount.$key"] = "required";
        }

        return $validatArr;
    }

    public function messages()
    {
      $messages = [];
      foreach($this->request->get('sku') as $key => $val)
      {
        $messages['sku.'.$key.'.exists'] = 'The sku item is not valid';
      }
      foreach($this->request->get('quantity') as $key => $val)
      {
        $messages['quantity.'.$key] = 'Quantity field is required';
      }
      foreach($this->request->get('sku') as $key => $val)
      {
        $messages['discount.'.$key] = 'Discount field is required';
      }

      //shipping error messages handler
      $messages["shipping_first_name.required_if"] = "First name is required";
      $messages["shipping_last_name.required_if"] = "Last name is required";
      $messages["shipping_address_1.required_if"] = "Shipping address is required";
      $messages["shipping_city.required_if"] = "Shipping city is required";
      $messages["shipping_state.required_if"] = "Shipping state is required";
      $messages["shipping_country.required_if"] = "Shipping country is required";
      $messages["shipping_postcode.required_if"] = "Shipping postcode is required";
      $messages["credit_card_exp.max"] = "Invalid Month";


      return $messages;
    }
}
