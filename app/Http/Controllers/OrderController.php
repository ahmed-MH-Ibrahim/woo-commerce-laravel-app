<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Customer;
use App\Order;
use App\OrdersDetails;
use App\Product;
use App\Website;
use Validator;
use DB;
use Session;
use App\Http\Requests\OrderRequest;
use Automattic\WooCommerce\Client;
use Braintree_Configuration;
use Braintree_ClientToken;
use Braintree_Transaction;


//require_once("/../../Braintree.php");


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    public function createOrder($id)
    {

        if(!Auth::check())
            return view("auth.login");

        $customer = Customer::find($id);

        if($customer == null)
        {
            Session::flash('error', "No valid customer selected");
            $customers = DB::table('customers')->paginate(15);

            $data = ["customers" => $customers];

            return view("dashboard.customers",$data);
        }


        $websites = Website::all();

        $websiteList = [];

        foreach ($websites as $key => $value) {
            $websiteList[$value["id"]]  =$value["name"];
        }

        $products = Product::all();

        $skuList = [];
        $productnameList = [];
        $productJSList = [];
        $skuJSList = [];

        foreach ($products as $key => $value) {
            $skuList[$value["sku"]]  =$value["sku"];
            $productnameList[$value["sku"]]  =$value["name"];
            $productJSList[] = $value["name"];
            $skuJSList[] = $value["sku"];
        }

        

        $data = ["customer"=> $customer, "websiteList" => $websiteList, "skuList" => $skuList, 
        "productnameList" => $productnameList, "products" => $products, "productJSList" => $productJSList,
        "site_shipping_type" => config("websites.site_shipping_type"), "skuJSList" => $skuJSList];

        return view("dashboard.add-order", $data);
    }

    function woocommerceApiConnect($siteId)
    {
        $site = Website::find($siteId);


        if($site == null)
            return null;

        $site = $site->name;

        /*$woocommerce = new Client(
            'http://find-my-product.com/pastries', 
            'ck_40599b80d724af16b00faacf59cfda5cef1d3784', 
            'cs_0fbf5da7bcf3779abb8160b7fa2fa195714c0794',
            [
                'wp_api' => true,
                'version' => 'wc/v2', //check if rest_no_route case happen
            ]
        );

        return $woocommerce;*/


        if($site != null && trim($site) != '' && isset(config('websites.woocommerce_api')[$site]) && count(config('websites.woocommerce_api')[$site]) >= 2)
        {
            $woocommerce = new Client('https://'.$site,config('websites.woocommerce_api')[$site][0],config('websites.woocommerce_api')[$site][1],
                [
                    'wp_api' => true,
                    'version' => 'wc/v2', //check if rest_no_route case happen
                ]
            );  

            return $woocommerce;
        }

        return null;
    }

    public function exportOrder($order,$customer,$request)
    {

        $request = $request->all(); 

        
        $woocommerce = $this->woocommerceApiConnect($request["website_id"]);

        if($woocommerce == null)
        {
            Session::flash('error', "Failed to export data, invalid site");
            return redirect("/order/newOrder/".$data['customer-id'])->withInput($request);
        }
             

        //add customer, than order
        //get customer and check if exists first
        $customer_exists = $woocommerce->get('customers',["email" => "$customer->email"]);


        //customer doesn't exist
        if(empty($customer_exists))
        {

            $data = [
                'email' => $customer->email,
                'first_name' => $customer->first_name,
                'last_name' => $customer->last_name,
                'password' => $customer->first_name."_".$customer->last_name, //required
            ];

            $data["billing"] =  [
                'first_name' => $customer->first_name,
                'last_name' => $customer->last_name,
                'address_1' => $customer->address_1,
                'address_2' => $customer->address_2 == null ? "":$customer->address_2,
                'city' => $customer->city,
                'state' => $customer->state,
                'postcode' => $customer->zip,
                'country' => $customer->country,
                'email' => $customer->email,
                'phone' => $customer->phone
            ];

            $data["shipping"] = $data["billing"];

            $woocommerce->post('customers', $data);
        }
       
        $data = [];

        $data = [
            'payment_method' => 'paypal_pro',
            'payment_method_title' => 'Pay by credit card',
            'set_paid' => $order->status == "completed" ? true:false,
        ];

        $data["billing"] =  [
            'first_name' => $customer->first_name,
            'last_name' => $customer->last_name,
            'address_1' => $customer->address_1,
            'address_2' => $customer->address_2 == null ? "":$customer->address_2,
            'city' => $customer->city,
            'state' => $customer->state,
            'postcode' => $customer->zip,
            'country' => $customer->country,
            'email' => $customer->email,
            'phone' => $customer->phone
        ];

        $useShipping = true;
        if($order->shipping_address_1 == null)
            $useShipping = false;

        if(!$useShipping)
        {
            $data["shipping"] = $data["billing"] ;
        }
        else
        {
            $data["shipping"] =  [
                'first_name' => $order->shipping_first_name,
                'last_name' => $order->shipping_last_name,
                'address_1' => $order->shipping_address_1,
                'address_2' => $order->shipping_address_2 == null ? "":$order->shipping_address_2,
                'city' => $order->shipping_city,
                'state' => $order->shipping_state,
                'postcode' => $order->shipping_postcode,
                'country' => $order->shipping_country
            ];
        }

        $data["line_items"] = [];
        $orderDetails = $order->details;
        foreach ( $orderDetails as $key => $value) {
            # code...
            $data["line_items"][] = ["sku" => $value->sku,"quantity"=> $value->quantity];
        }

        if( $request['shipping_method'] != null && isset(config('websites.shipping_method')[$request['shipping_method']]) )
        {
            $data["shipping_lines"] = [];

            $methodID = $order->shipping_cost == 0 ? "free_shipping":"flat_rate";
            $data["shipping_lines"][] = ['method_id' => $methodID,
                                    'method_title' => config('websites.shipping_method')[$request['shipping_method']]];
        }

        $data["status"] = $order->status;

        $orderExported = $woocommerce->post('orders', $data);

        $order->order_id = $orderExported["id"];
        $order->update();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        if(!Auth::check())
            return redirect("/");

        $data = $request->all();

        $customer = null;

        $customer = Customer::find($data['customer-id']);

        if($customer == null)
        {
            Session::flash('error', "Customer doesn't exist");
            return redirect("/");
        }

        $woocommerce = $this->woocommerceApiConnect($request["website_id"]);

        if($woocommerce == null)
        {
            Session::flash('error', "Invalid site value. Can't connect to wordpress website");
            return redirect("/order/newOrder/".$data['customer-id'])->withInput($request->all());
        }

        //checking shipping method
        /*$shipping_method_exists = $woocommerce->get('shipping_methods', ['id' => $data["shipping_method"] ]);

        if(empty($shipping_method_exists))
        {
            Session::flash('error', "Shipping method doesn't exist in selected website ");
                
            return redirect("/order/newOrder/".$data['customer-id'])->withInput($request->all());
        }*/

        $order = new Order();

        $order->website_id = $data['website_id'];
        $order->customer_id = $data['customer-id'];
        $order->order_date = date("Y-m-d H:i:s");
        $order->status = $data['status'];
        $order->shipping_first_name = $data['shipping_first_name'];
        $order->shipping_last_name = $data['shipping_last_name'];
        $order->shipping_address_1 = $data['shipping_address_1'];
        $order->shipping_address_2 = $data['shipping_address_2'];
        $order->shipping_postcode = $data['shipping_postcode'];
        $order->shipping_city = $data['shipping_city'];
        $order->shipping_state = $data['shipping_state'];
        $order->shipping_country = $data['shipping_country'];
        $order->shipping_company = "";//$data['shipping_company'];
        $order->shipping_cost = $data['shipping_cost'];
        $order->tax = $data['tax'];
        $order->discount = $data['total_discount'];

        //calculate subtotal and total charged
        $order->subtotal = 0;

        $orderDetails = [];


        foreach ($data["sku"] as $key => $value) {

            //check product exists on site
            $sku = $value;
            $product_exists = $woocommerce->get('products',['sku' => $sku]);


            //customer doesn't exist
            if(empty($product_exists))
            {
                Session::flash('error', "Product with sku $sku isn't found in selected website ");
                
                return redirect("/order/newOrder/".$data['customer-id'])->withInput($request->all());
            }

            //get product
            $product = Product::where("sku",$value)->first();

            $order->subtotal += (float)$data['quantity'][$key]*(float)$product->unit_price*(float)(1-(float)$data["discount"][$key]/(float)100);

            $orderDetail = new OrdersDetails();
            $orderDetail->sku = $value;
            $orderDetail->quantity = $data['quantity'][$key];
            $orderDetail->discount = $data['discount'][$key];
            $orderDetail->unit_price = $data['unit_price'][$key];
            $orderDetail->website_id = $data['website_id'];

            $orderDetails[] = $orderDetail;
        }

        $order->subtotal = number_format((float)$order->subtotal, 2, '.', '');

        $order->total_charged = $order->subtotal+$data['tax']+$data['shipping_cost']-$data['total_discount'];

        $order->total_charged = number_format((float)$order->total_charged, 2, '.', '');

        



        
        if(!$this->braintreePurchase($order,$customer,$request))
        {
            return redirect("/order/newOrder/".$data['customer-id']);
        }

        $this->exportOrder($order,$customer,$request);

        $order->save();

        //save order details

        foreach ($orderDetails as $key => $value) {
            $value->order_id = $order->id;
            $value->save();
        }

        Session::flash('success', "Order added successfully");
        return redirect("/customer/".$data['customer-id']."/edit");
    }

    public function braintreePurchase($order,$customer,$request)
    {
        Braintree_Configuration::environment(config('braintree.environment'));
        Braintree_Configuration::merchantId(config('braintree.merchantId'));
        Braintree_Configuration::publicKey(config('braintree.publicKey'));
        Braintree_Configuration::privateKey(config('braintree.privateKey'));

        //$clientToken = Braintree_ClientToken::generate();

        $data = $request->all();

        $site = Website::find($order->website_id);

        $site = $site->name;

        $nonceFromTheClient = $data["payment_method_nonce"];

        $useShipping = true;

        if($order->shipping_address_1 == null)
            $useShipping = false;

        $billing = [];

        if($useShipping)
        {
            $billing = [
                'firstName' => $order->shipping_first_name,
                'lastName' => $order->shipping_last_name,
                'postalCode' => $order->shipping_postcode
            ];
        }
        else
        {
            $billing = [
                'firstName' => $customer->first_name,
                'lastName' => $customer->last_name,
                'postalCode' => $customer->zip
            ];
        }

        $expirationDate = substr($data['credit_card_exp'],0,2).'/'.substr($data['credit_card_exp'],2,2);
        $result = Braintree_Transaction::sale([
          'amount' => $order->total_charged,
          //'paymentMethodNonce' => $clientToken,//$nonceFromTheClient,//"fake-valid-nonce",
          'creditCard' => [
            "number" => $data['credit_card'],
            'cvv' => $data['credit_card_cvn'],
            'expirationDate' => $expirationDate
          ],
          'options' => [
            'submitForSettlement' => True
          ],
          'billing' => $billing,
          'descriptor' => [
            'name' => config("websites.braintree_info")[$site]["descriptor_name"],
            'phone' => config("websites.braintree_info")[$site]["descriptor_phone"],
            'url' => config("websites.braintree_info")[$site]["descriptor_url"],
            ],
            'merchantAccountId' => config("websites.braintree_info")[$site]["merchant_id"],
        ]);

        if ($result->success || !is_null($result->transaction)) {
            $transaction = $result->transaction;
             //var_dump($transaction->id);

             $order->approval_code = $transaction->id;
             $order->update();
             //dd($transaction);
            //header("Location: transaction.php?id=" . $transaction->id);
        } else {
            $errorString = "";
            foreach($result->errors->deepAll() as $error) {
                $errorString .= 'Error: ' . $error->code . ": " . $error->message . "\n";
            }


            //dd($result);

           // $_SESSION["errors"] = $errorString;
            Session::flash('error', $errorString);
            return false;
            //dd($errorString);
            //header("Location: index.php");
        }

        return true;
       
    }

    public function braintreeRefund(Request $request,$id)
    {
        if(!Auth::check())
            return redirect("/");

        $order = Order::find($id);

        if($order == null )
        {
            Session::flash('error', "Refund request to invalid order");
            return redirect("/orders");
        }

        if($order['status'] == "completed")
        {
            Session::flash('error', "Can't edit a completed order");
            return redirect("/order/".$id."/edit");
        }

        if($order->approval_code == null)
        {
            Session::flash('error', "Invalid transaction id");
            return redirect("/order/".$id."/edit");
        }

        $validator = Validator::make($request->all(),[
            'refund' => "required|numeric"
        ]);

        
        if($validator->fails())
        {
            $this->throwValidationException($request, $validator); 
        }

        $data = $request->all();

        Braintree_Configuration::environment(config('braintree.environment'));
        Braintree_Configuration::merchantId(config('braintree.merchantId'));
        Braintree_Configuration::publicKey(config('braintree.publicKey'));
        Braintree_Configuration::privateKey(config('braintree.privateKey'));

        $result = Braintree_Transaction::refund($order->approval_code, $data["refund"]);

        if($result->success)
        {
            Session::flash('success', "Refund request submitted successfully");
            $order->status = "refunded";
            $order->update();
            return redirect("/order/".$id."/edit");
        }
        else{
            Session::flash('error', "$result->message");
            return redirect("/order/".$id."/edit");
        }

        //dd($result);
       
    }

    public function braintreeVoid(Request $request,$id)
    {
        if(!Auth::check())
            return redirect("/");

        $order = Order::find($id);

        if($order == null)
        {
            Session::flash('error', "Void request to invalid order");
            return redirect("/orders");
        }

        if($order['status'] == "completed")
        {
            Session::flash('error', "Can't edit a completed order");
            return redirect("/order/".$id."/edit");
        }

        if($order->approval_code == null)
        {
            Session::flash('error', "Invalid transaction id");
            return redirect("/order/".$id."/edit");
        }

        $data = $request->all();

        Braintree_Configuration::environment(config('braintree.environment'));
        Braintree_Configuration::merchantId(config('braintree.merchantId'));
        Braintree_Configuration::publicKey(config('braintree.publicKey'));
        Braintree_Configuration::privateKey(config('braintree.privateKey'));

        $result = Braintree_Transaction::void($order->approval_code);

        if($result->success)
        {
            Session::flash('success', "Void request submitted successfully");
            $order->status = "cancelled";
            $order->update();
            return redirect("/order/".$id."/edit");
        }
        else{
            Session::flash('error', "$result->message");
            return redirect("/order/".$id."/edit");
        }

        //dd($result);
       
    }

    public function addOrderDetails(Request $request)
    {

    }

    public function refunds($id)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::check())
            return view("auth.login");

        $order = Order::find($id);

        if($order == null /*|| $order['status'] == "completed"*/)
        {            
            return redirect("/orders");
        }

        $websites = Website::all();

        $websiteList = [];

        foreach ($websites as $key => $value) {
            $websiteList[$value["id"]]  =$value["name"];
        }

        $productsList = [];
        $ordersList = $order->details;
        foreach( $ordersList as $orderDetail)
        {
            $product = Product::where("sku",$orderDetail['sku'])->first();
            //var_dump($product['name']);
            if($product != null)
                $productsList[$orderDetail['sku']] = $product["name"];
        }

        //var_dump($productsList);


        $data = ["order" => $order, "id"=> $id, "websiteList" => $websiteList, "productsList" => $productsList];

        return view("dashboard.edit-order",$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::check())
            return redirect("/");

        $order = Order::find($id);

        if($order == null )
        {
            Session::flash('error', "Invalid order");
            return redirect("/orders");
        }

        if($order['status'] == "completed")
        {
            Session::flash('error', "Can't edit a completed order");
            return redirect("/order/".$id."/edit");
        }

        $validator = Validator::make($request->all(),[
            'website_id' => "required",
            'status' => "required"
        ]);

        
        if($validator->fails())
        {
            $this->throwValidationException($request, $validator); 
        }

        $data = $request->all();

        $exportToSite = false;
        if($order->website_id != $data["website_id"])
            $exportToSite = true;

        //update order
        $order->website_id = $data["website_id"];
        $order->status = $data["status"];

        $order->update();

        $orderDetails = $order->details;

        foreach ($orderDetails as $key => $value) {
            $value->website_id = $order->website_id;
            $value->update();
        }



        /*if($exportToSite)
        {
            //get customer
            $customer = Customer::find($order->customer_id);
            $this->exportOrder($order,$customer,$request);
        }*/

        Session::flash('success', "Order updated successfully");
        return redirect("/order/".$id."/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
