<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Customer;
use App\Order;
use App\OrdersDetails;
use App\Product;
use App\Website;
use Validator;
use DB;
use Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("dashboard.add-customer");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::check())
            return redirect("/");

        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'first_name' => "required",
            'last_name' => "required",
            'address_1' => "required", 
            'city' => "required",
            'state' => "required",
            'zip' => "required",
            'country' => "required",
            'phone' => "required",
            'email' => "required|email",
        ]);

        //'address_2',
        //'comments'
        //company
        //website_id => set to default 1

        
        if($validator->fails())
        {
            $this->throwValidationException($request, $validator); 
        }


        $data = $request->all();


        $data["website_id"] = "1";

        $customer = new Customer($data);
        $customer->save();

        Session::flash('success', "User $data[first_name] was added successfully");
        return redirect("/customer/".$customer->id."/edit");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function search(Request $request)
    {
        if(!Auth::check())
            return response()->json(["error" => "Unauthorized access"]);

        $data = $request->all();

        if(( !isset($data['lastname']) || $data['lastname'] == null || trim($data['lastname']) == '' )
            && ( !isset($data['firstname']) || $data['firstname'] == null || trim($data['firstname']) == '' )
            && ( !isset($data['city']) || $data['city'] == null || trim($data['city']) == '' ) 
            && ( !isset($data['phone']) || $data['phone'] == null || trim($data['phone']) == '' )
            && ( !isset($data['email']) || $data['email'] == null || trim($data['email']) == '' ))
            return response()->json(["error" => "Please set at least one field"]);

        $firstname = ( !isset($data['firstname']) || $data['firstname'] == null || trim($data['firstname']) == '' ) ? 'first_name LIKE "%"': "first_name like '%$data[firstname]%'";
        $lastname = ( !isset($data['lastname']) || $data['lastname'] == null || trim($data['lastname']) == '' ) ? 'last_name LIKE "%"': "last_name like '%$data[lastname]%'";
        $city = ( !isset($data['city']) || $data['city'] == null || trim($data['city']) == '' ) ? 'city LIKE "%"': "city like '%$data[city]%'";
        $phone = ( !isset($data['phone']) || $data['phone'] == null || trim($data['phone']) == '' ) ? 'phone LIKE "%"': "phone like '%$data[phone]%'";
        $email = ( !isset($data['email']) || $data['email'] == null || trim($data['email']) == '' ) ? 'email LIKE "%"': "email like '%$data[email]%'";

        $customers =  DB::Select("SELECT * FROM customers WHERE $firstname AND $lastname AND $city AND $phone AND $email");

        return response()->json($customers); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::check())
            return view("auth.login");

        $customer = Customer::find($id);

        if($customer == null)
            return redirect("/");

        $websites = Website::all();

        $websiteList = [];

        foreach ($websites as $key => $value) {
            $websiteList[$value["id"]]  =$value["name"];
        }
        

        $data = ["customer" => $customer, "id"=> $id, "websiteList" => $websiteList];

        return view("dashboard.edit-customer",$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::check())
            return redirect("/");

        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'first_name' => "required",
            'last_name' => "required",
            'address_1' => "required", 
            'city' => "required",
            'state' => "required",
            'zip' => "required",
            'country' => "required",
            'phone' => "required",
            'email' => "required|email",
        ]);

        //'address_2',
        //'comments'
        //company
        //website_id => set to default 1

        
        if($validator->fails())
        {
            $this->throwValidationException($request, $validator); 
        }


        $data = $request->all();


        $data["website_id"] = "1";

        $customer = Customer::find($id);
        $customer->first_name = $data["first_name"];
        $customer->last_name = $data["last_name"];
        $customer->address_1 = $data["address_1"];
        $customer->address_2 = $data["address_2"];
        $customer->city = $data["city"];
        $customer->state = $data["state"];
        $customer->zip = $data["zip"];
        $customer->country = $data["country"];
        $customer->phone = $data["phone"];
        $customer->email = $data["email"];
        $customer->comments = $data["comments"];
        $customer->website_id = $data["website_id"];
        $customer->update();

        Session::flash('success', "User updated successfully");
        return redirect("/customer/$id/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
