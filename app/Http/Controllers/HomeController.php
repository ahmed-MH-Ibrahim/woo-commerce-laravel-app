<?php
namespace App\Http\Controllers;


error_reporting(E_ALL);

//require __DIR__ . '/vendor/autoload.php';

use Automattic\WooCommerce\Client;

use Illuminate\Http\Request;

use Auth;
use App\Customer;
use App\Order;
use App\OrdersDetails;
use App\Product;
use App\Website;
use Validator;
use DB;

class HomeController extends Controller
{
    use \App\Traits\ImporterJob;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard()
    {
        if(!Auth::check())
            return view("auth.login");

        $websites = Website::all();

        $websiteList = [];

        foreach ($websites as $key => $value) {
            $websiteList[$value["id"]]  =$value["name"];
        }

        $data = ["websiteList" => $websiteList];

        return view("dashboard.dashboard",$data);
    }

    public function LoadCustomers()
    {
        if(!Auth::check())
            return view("auth.login");

        $customers = DB::table('customers')->paginate(15);

        $data = ["customers" => $customers];

        return view("dashboard.customers",$data);
    }

    public function loadProducts(){
        if(!Auth::check())
            return view("auth.login");

        $products = DB::table('products')->paginate(15);

        $websites = Website::all();

        $websiteList = [];

        foreach ($websites as $key => $value) {
            $websiteList[$value["id"]]  =$value["name"];
        }

        $data = ["products" => $products, "websiteList" => $websiteList];

        return view("dashboard.products",$data);
    }

    public function LoadOrders()
    {
        if(!Auth::check())
            return view("auth.login");

        //$orders = Order::all();
        $orders = DB::table('orders')->paginate(15);

        $websites = Website::all();

        $websiteList = [];

        foreach ($websites as $key => $value) {
            $websiteList[$value["id"]]  =$value["name"];
        }

        $data = ["orders" => $orders, "websiteList" => $websiteList];

        return view("dashboard.orders",$data);
    }



    public function demoOrder(){

        /*
        'https://greghowlett.com/', 
            'ck_1841b861972c6fa7eefb9c681e91c8435e9b50e1', 
            'cs_d70ced9a95de60fee9c80cedb65e6f6ae1913968',
        */

        $woocommerce = new Client(
            'http://find-my-product.com/pastries', 
            'ck_40599b80d724af16b00faacf59cfda5cef1d3784', 
            'cs_0fbf5da7bcf3779abb8160b7fa2fa195714c0794',
            [
                'wp_api' => true,
                'version' => 'wc/v2', //check if rest_no_route case happen
            ]
        );

        $data = [
            'payment_method' => 'bacs',
            'payment_method_title' => 'Direct Bank Transfer',
            'set_paid' => true,
            'billing' => [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'address_1' => '969 Market',
                'address_2' => '',
                'city' => 'San Francisco',
                'state' => 'CA',
                'postcode' => '94103',
                'country' => 'US',
                'email' => 'john.doe@example.com',
                'phone' => '(555) 555-5555'
            ],
            'shipping' => [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'address_1' => '969 Market',
                'address_2' => '',
                'city' => 'San Francisco',
                'state' => 'CA',
                'postcode' => '94103',
                'country' => 'US'
            ],
            'line_items' => [
                [
                    'sku' => 'CUP-04',
                    'quantity' => 2
                ],
                [
                    'sku' => 'Cake-01',
                    //'variation_id' => 23,
                    'quantity' => 1
                ]
            ],
            /*'shipping_lines' => [
                [
                    'method_id' => 'flat_rate',
                    'method_title' => 'Flat Rate',
                    'total' => 10
                ]
            ]*/
        ];

        print_r($woocommerce->post('orders', $data));


    }

    public function shippingMethodsDemo()
    {
        

        $woocommerce = new Client(
            'http://caringsunshine.com', 
            'ck_112cdb903fd8efdf75a8ee664b68b553e36fdd52', 
            'cs_690378d84c998275106bdaa5b293fdd9c93a968d',
            [
                'wp_api' => true,
                'version' => 'wc/v2', //check if rest_no_route case happen
            ]
        );

        //echo "<pre>";
        dd($woocommerce->get('shipping_methods'));
        //echo "</pre>";
    }

     public function demoCustomer(){

        $woocommerce = new Client(
            'http://find-my-product.com/pastries', 
            'ck_40599b80d724af16b00faacf59cfda5cef1d3784', 
            'cs_0fbf5da7bcf3779abb8160b7fa2fa195714c0794',
            [
                'wp_api' => true,
                'version' => 'wc/v2', //check if rest_no_route case happen
            ]
        );    


        $data = [
            'email' => 'john.doe.custom@example.com',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'password' => 'test', //required
            //'username' => 'john.doe',
            'billing' => [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'company' => '',
                'address_1' => '969 Market',
                'address_2' => '',
                'city' => 'San Francisco',
                'state' => 'CA',
                'postcode' => '94103',
                'country' => 'US',
                'email' => 'john.doe@example.com',
                'phone' => '(555) 555-5555'
            ],
            'shipping' => [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'company' => '',
                'address_1' => '969 Market',
                'address_2' => '',
                'city' => 'San Francisco',
                'state' => 'CA',
                'postcode' => '94103',
                'country' => 'US'
            ]
        ];

        print_r($woocommerce->post('customers', $data));

    }

    public function csvImport(Request $request){

        if(!Auth::check())
            return view("auth.login");

        //var_dump($request->all());

        if (!$request->hasFile('file')) {
            return response()->json(["error" => "No file found !"]);
        }

        $lineCounter=0;
        $exceptionMsg = "";
        
        try{

            $sites = config('websites.sites');

            //var_dump($sites);

            $csvFile = $request->file('file');

            $filename = $csvFile->getClientOriginalName();
            $short = substr($filename, 0, 3);

            //var_dump($sites);
            //var_dump($short);

            if(!array_key_exists($short,$sites))
            {
                return response()->json(["error" => "Site Source not recognized"]); 
            }

            //Convert data to database
            $csv = array_map('str_getcsv', file($csvFile));

            //var_dump($csv);


            $sites = Website::all();
            $websites = [];

            foreach ($sites as $row) {
                $websites[$row->short] = $row->id;
            }

            //var_dump($websites);

            $headers = [];
            

            foreach ($csv as $line) {

                //set headers            
                if($lineCounter++==0)
                {
                    //get headers
                    
                    foreach ($line as $key => $value) {
                        $headers[$value] = $key;
                    }

                    continue;
                }
                    
                //var_dump($headers);
                //var_dump($line);
                //upsert customer

                $customer = Customer::where('email', $line[$headers['billing_email']])->first();

                if($customer == null)
                {
                    //create customer
                    $customer = new Customer();

                    $customer->customer_id = $line[$headers['customer_id']];
                    $customer->first_name = $line[$headers['billing_first_name']];
                    $customer->website_id = $websites[$short];
                    $customer->last_name = $line[$headers['billing_last_name']];
                    $customer->address_1 = $line[$headers['billing_address_1']];
                    $customer->address_2 = $line[$headers['billing_address_2']];
                    $customer->city = $line[$headers['billing_city']];
                    $customer->state = $line[$headers['billing_state']];
                    $customer->zip = $line[$headers['billing_postcode']];
                    $customer->country = $line[$headers['billing_country']];
                    $customer->company = $line[$headers['billing_company']];

                    /*$customer->shipping_first_name = $line[$headers['shipping_first_name']];
                    $customer->website_id = $websites[$short];
                    $customer->shipping_last_name = $line[$headers['shipping_last_name']];
                    $customer->shipping_address_1 = $line[$headers['shipping_address_1']];
                    $customer->shipping_address_2 = $line[$headers['shipping_address_2']];
                    $customer->shipping_city = $line[$headers['shipping_city']];
                    $customer->shipping_state = $line[$headers['shipping_state']];
                    $customer->shipping_postcode = $line[$headers['shipping_postcode']];
                    $customer->shipping_country = $line[$headers['shipping_country']];
                    $customer->shipping_company = $line[$headers['shipping_company']];*/

                    $customer->phone = $line[$headers['billing_phone']];
                    $customer->email = $line[$headers['billing_email']];
                    $customer->comments = $line[$headers['customer_note']];
                    $customer->save();
                }

                //upsert product
               // echo "product SKU = ".$line[$headers['item_sku']];
                if(trim($line[$headers['item_sku']]) == '')
                {
                    $this->errorFound("Empty SKU field, row $lineCounter",$filename);
                    continue;
                }

                $product = Product::where("sku", $line[$headers['item_sku']])->first();


                if($product == null)
                {
                    $product = new Product();
                    $product->name = $line[$headers['item_name']];
                    $product->sku = $line[$headers['item_sku']];
                    $product->unit_price = $line[$headers['item_subtotal']];
                    $product->weight = $line[$headers['item_weight']];
                    $product->website_id = $websites[$short];
                    $product->save();
                }

                //upsert order
                $order = Order::where("order_id", $line[$headers['order_id']])
                                ->where("website_id",$websites[$short])
                                ->first();

                $newOrder = false;

                if($order == null)
                {
                    $order = new Order();
                    $order->customer_id = $customer->id;
                    $order->order_id = $line[$headers['order_id']];
                    $order->order_date = $line[$headers['date']];
                    $order->status = $line[$headers['status']];
                    $order->subtotal = 0;//(float)$line[$headers['item_subtotal']]*(float)$line[$headers['item_quantity']];
                    $order->discount = $line[$headers['discount_total']];
                    $order->shipping_cost = $line[$headers['shipping_total']];
                    $order->tax = $line[$headers['tax_total']];
                    $order->total_charged = $line[$headers['order_total']];
                    $order->approval_code = $line[$headers['BraintreeApprovalCode']];
                    $order->credit_card = '';
                    $order->website_id = $websites[$short];

                    $order->shipping_first_name = $line[$headers['shipping_first_name']];
                    $order->website_id = $websites[$short];
                    $order->shipping_last_name = $line[$headers['shipping_last_name']];
                    $order->shipping_address_1 = $line[$headers['shipping_address_1']];
                    $order->shipping_address_2 = $line[$headers['shipping_address_2']];
                    $order->shipping_city = $line[$headers['shipping_city']];
                    $order->shipping_state = $line[$headers['shipping_state']];
                    $order->shipping_postcode = $line[$headers['shipping_postcode']];
                    $order->shipping_country = $line[$headers['shipping_country']];
                    $order->shipping_company = $line[$headers['shipping_company']];

                    $order->save();

                    $newOrder = true;
                }


                //upsert order_details

                //Note: The order id I use for order details is the local order's id not the imported one (external site's order id)

                $orderDetails = OrdersDetails::where("order_id", $order->id)
                                ->where("website_id",$websites[$short])
                                ->where("sku",$line[$headers['item_sku']])
                                ->first();

                if($orderDetails == null)
                {
                    $orderDetails = new OrdersDetails();

                    $orderDetails->order_id = $order->id;
                    $orderDetails->sku = $product->sku;
                    $orderDetails->quantity = $line[$headers['item_quantity']];
                    $orderDetails->unit_price = $line[$headers['item_subtotal']];
                    $orderDetails->website_id = $websites[$short];

                    if((float)$line[$headers['item_subtotal']] == 0)
                        $orderDetails->discount = 0;
                    else
                        $orderDetails->discount = (float)(((float)$line[$headers['discount_total']]/(float)$line[$headers['item_subtotal']])*100);
                    $orderDetails->save();


                    //add to order's subtotal
                    $order->subtotal += (float)$line[$headers['item_subtotal']];
                    $order->update();
                    /*$order->total_charged = $order->subtotal-$order->discount+$order->tax+$order->shipping_cost;
                    ;*/

                        
                }   


            }
        } catch (Exception $e) {

            $csvFile = $request->file('file');

            $filename = $csvFile->getClientOriginalName();

            //Send email

            //return error

            return response()->json(["error" => "Exception: ".$e->getMessage()." From file $filename. Orders processed ".($lineCounter-1)]);
        }
        //return response()->json(["success" => "true"]);
    }

    /*function errorFound($message,$filename)
    {
        $to = "greghowlett@gmail.com";
        $subject = "Error reporting for file $filename";

        $text= "<br/><strong>File: </strong>".$filename." \r\n <br/><br/>";
        $text.= "<br/><strong>Error: </strong>".$message." \r\n <br/><br/>";


        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 

        //var_dump($message);
       // $mailsent = mail($to,$subject,$text,$headers);
    }*/

}
