<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/plugins/jQueryUI/jquery-ui.css')}}" rel="stylesheet">
    <link href="{{asset('template/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/plugins/dropzone/basic.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/plugins/dropzone/dropzone.css')}}" rel="stylesheet">


    @yield('styles')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">


</head>


<body class="pace-done">
    <div id="loading"><img src="{{asset('img/loading.gif')}}" width="50px"></div>
    <div id="wrapper" class="wrapper container-fluid">


        @if(!Request::is('login') )
          @include('layouts.sidebar')
        @endif
        <div id="" class="gray-bg col-md-10 col-sm-9 col-xs-12">
            @yield('content')
        </div>
        @include('layouts.footer')
    </div>

    <!-- Scripts -->
    <script src="{{asset('template/js/jquery-2.1.1.js')}}"></script>
    <script src="{{asset('template/js/jquery-ui-1.10.4.min.js')}}"></script>
    <script src="{{asset('template/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('template/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('template/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset('template/js/inspinia.js')}}"></script>
    <script src="{{asset('template/js/plugins/pace/pace.min.js')}}"></script>
    
    @yield('scripts')

    <script src="{{ asset('js/main.js') }}"></script>

</body>
</html>


   
