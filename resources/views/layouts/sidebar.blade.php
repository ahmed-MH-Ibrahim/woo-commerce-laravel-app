
<nav class="navbar-default col-md-2 col-md-offset-0 col-sm-3 col-sm-offset-0 col-xs-offset-0 col-xs-12" role="navigation" style=";margin-top:20px">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">            
            <li>
                <a href="{{url('/')}}"><i class="fa fa-user"></i> <span class="nav-label">Customers</span></a>
            </li>
            <li>
                <a href="{{url('/orders')}}"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Orders</span></a>
            </li>
            <li>
                <a href="{{url('/products')}}"><i class="fa fa-gift"></i> <span class="nav-label">Products</span></a>
            </li>
           <!--  <li>
                <a href="{{url('/dashboard')}}"><i class="fa fa-wrench"></i> <span class="nav-label">Manage Content</span></a>
            </li> -->
            <li>
                <a href="{{url('/import')}}"><i class="fa fa-download"></i> <span class="nav-label">Import</span></a>
            </li>
            <li>
                <a href="{{url('/cronjob/import')}}"><i class="fa fa-download"></i> <span class="nav-label">Run cron script</span></a>
            </li>
        </ul>
    </div>
</nav>