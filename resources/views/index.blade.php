@extends('layouts.app')

@section('scripts')

<script src="{{asset('template/js/plugins/dropzone/dropzone.js')}}"></script>
<script>
    jQuery(document).ready(function($){

        Dropzone.options.myAwesomeDropzone = {

            autoProcessQueue: false,
            uploadMultiple: false,
            //maxFiles: 1,
            acceptedFiles: ".csv",
            filesizeBase: 8000,
            maxFilesize: 1024,

            // Dropzone settings
            init: function() {
                var myDropzone = this;

                this.element.querySelector("input[type=submit]").addEventListener("click", function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();


                });
                this.on("success", function(files,response) {
                    console.log(files);
                    console.log(response);
                    //myDropzone.disable();

                    //files.status = Dropzone.QUEUED;

                    if(response.success != undefined)
                    {
                        window.location = "{{ url('dashboard') }}";
                    }
                    else if(response.error)
                    {
                        //$("#error").empty();
                        $("#error").append("<div>"+response.error+"</div><br/>");
                        $("#error").show();
                    }
                });              
            },

        }

   });
</script>



@endsection

@section('content')



<div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Importer</h5>
                    </div>
                    <div class="ibox-content">
                        {{ Form::open(
                            array(
                                'url' => '/import', 
                                'class' => 'form dropzone', 
                                'id' => 'my-awesome-dropzone',
                                'role'=>'form', 'enctype'=>"multipart/form-data",
                                'files' => true,
                                'method' => 'post')) 
                        }}


                            <div class="dropzone-previews"></div>

                            <div class="form-group">
                                {{ Form::submit('Upload CSV File', ['class' => 'btn btn-primary pull-right']) }}
                            </div>
                            <!-- <button type="submit" class="btn btn-primary pull-right">Upload CSV File</button> -->

                        {{ Form::close() }}

                    </div>
                    <div class="alert alert-danger" id="error" style="display:none">

                    </div>
                </div>
            </div>
            </div>

        </div>

@endsection