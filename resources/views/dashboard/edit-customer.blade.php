@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12 col-md-offset-0 admin-panel add-category" style="margin-top:20px">
            @if (Session::has('error'))
               <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

             @if (Session::has('success'))
               <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif

            

            <div class="panel panel-default">
                
                <div class="panel-heading">Edit customer</div>                    
                <div class="panel-body">
                    {{ Form::open(['url' => '/customer/'.$id, 'class'=>' col-xs-12', 'method'=>'PUT', 'role'=>'form', 'enctype'=>"multipart/form-data"]) }}

                    <div class="form-group{{ $errors->has('customer_id') ? ' has-error' : '' }} col-sm-6 col-xs-12">
                        <label class="">Customer ID: </label>
                        <span class="">{{$customer['id']}}</span>
                        

                    </div>

                    <div class="form-group{{ $errors->has('updated_at') ? ' has-error' : '' }} col-sm-6 col-xs-12">
                        <label class="">Customer last update: </label>
                        <span class="">{{date('m/d/Y', strtotime($customer['updated_at']))}}</span>

                    </div>

                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} col-sm-6 col-xs-6">
                        <label>First name</label>
                        {{ Form::text('first_name',$customer['first_name'],['class'=>'form-control', 'placeholder'=>'First name', 'required' => 'required']) }}
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} col-sm-6 col-xs-6">
                        <label>Last name</label>
                        {{ Form::text('last_name',$customer['last_name'],['class'=>'form-control', 'placeholder'=>'Last name', 'required' => 'required']) }}
                        @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('address_1') ? ' has-error' : '' }} col-sm-6 col-xs-6">
                        <label>Address</label>
                        {{ Form::text('address_1',$customer['address_1'],['class'=>'form-control', 'placeholder'=>'Address', 'required' => 'required']) }}
                        @if ($errors->has('address_1'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address_1') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('address_2') ? ' has-error' : '' }} col-sm-6 col-xs-6">
                        <label>Address 2</label>
                        {{ Form::text('address_2',$customer['address_2'],['class'=>'form-control', 'placeholder'=>'Address 2']) }}
                        @if ($errors->has('address_2'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address_2') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }} col-sm-3 col-xs-6">
                        <label>City</label>
                        {{ Form::text('city',$customer['city'],['class'=>'form-control', 'placeholder'=>'City', 'required' => 'required']) }}
                        @if ($errors->has('city'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }} col-sm-3 col-xs-6">
                        <label>State</label>
                        {{ Form::text('state',$customer['state'],['class'=>'form-control', 'placeholder'=>'State', 'required' => 'required']) }}
                        @if ($errors->has('state'))
                            <span class="help-block">
                                <strong>{{ $errors->first('state') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }} col-sm-3 col-xs-6">
                        <label>Zip</label>
                        {{ Form::text('zip',$customer['zip'],['class'=>'form-control', 'placeholder'=>'Zip', 'required' => 'required']) }}
                        @if ($errors->has('zip'))
                            <span class="help-block">
                                <strong>{{ $errors->first('zip') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }} col-sm-3 col-xs-6">
                        <label>Country</label>
                        {{ Form::text('country',$customer['country'],['class'=>'form-control', 'placeholder'=>'country', 'required' => 'required']) }}
                        @if ($errors->has('country'))
                            <span class="help-block">
                                <strong>{{ $errors->first('country') }}</strong>
                            </span>
                        @endif
                    </div>

                    

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} col-sm-6 col-xs-6">
                        <label>Phone</label>
                        {{ Form::text('phone',$customer['phone'],['class'=>'form-control', 'placeholder'=>'Phone', 'required' => 'required']) }}
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-sm-6 col-xs-6">
                        <label>Email</label>
                        {{ Form::email('email',$customer['email'],['class'=>'form-control', 'placeholder'=>'Email', 'required' => 'required']) }}
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }} col-sm-12 col-xs-12">
                        <label>Comments</label>
                        {{ Form::textarea('comments',$customer['comments'],['class'=>'form-control', 'placeholder'=>'comments']) }}
                        @if ($errors->has('comments'))
                            <span class="help-block">
                                <strong>{{ $errors->first('comments') }}</strong>
                            </span>
                        @endif
                    </div>

                   

                    <div class="form-group  col-xs-12">
                        <a class="btn btn-primary" href="{{url('/order/newOrder/'.$id)}}">Add Order</a>
                        {{ Form::submit('Save', ['class' => 'btn btn-primary fr']) }}
                    </div>

                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">{{$customer["first_name"]}}'s Orders</div>  
        <div class="panel-body">
            <table class="table table-striped tanksTable">
                <thead ng-if="data.length > 0">
                    <tr>   
                        <th>Order id</th>
                        <th>Order date</th>
                        <th>Website</th>
                        <th>Status</th>
                        <th>Discount</th>
                        <th>Order Total</th>                                    
                    </tr>
                </thead>
                <tbody>
                    @if($customer->orders == null)
                        <tr>
                            <td title="No orders found" >No orders found</td>
                            <td title="No orders found" >  </td>
                            <td title="No orders found" >  </td>
                            <td title="No orders found" >  </td>
                            <td title="No orders found" >  </td>
                            <td title="No orders found" >  </td>
                        </tr>
                    @else
                       @foreach($customer->orders as $order)

                                <tr>         
                                    <td title="{{$order['id']}}" ><a href='{{url("order/$order[id]/edit")}}'>{{$order['id']}}</a></td>
                                    <td title="{{$order['order_date']}}" ><a href='{{url("order/$order[id]/edit")}}'>{{date('m/d/Y', strtotime($order['order_date']))}}</a></td>
                                    <td title="{{$websiteList[$order['website_id']]}}" ><a href='{{url("order/$order[id]/edit")}}'>{{$websiteList[$order['website_id']]}}</a></td>
                                    <td title="{{$order['status']}}" ><a href='{{url("order/$order[id]/edit")}}'>{{$order['status']}}</a></td>   
                                    <td title="{{$order['discount']}}" ><a href='{{url("order/$order[id]/edit")}}'>${{number_format((float)$order['discount'], 2, '.', '')}}</a></td>   
                                    <td title="{{$order['total_charged']}}" ><a href='{{url("order/$order[id]/edit")}}'>${{number_format((float)$order['total_charged'], 2, '.', '')}}</a></td>                                 
                                </tr> 
                         
                        @endforeach  
                    @endif
                </tbody> 
            </table>

        </div>
    </div>

@endsection
