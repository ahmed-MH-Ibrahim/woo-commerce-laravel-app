@extends('layouts.app')

@section('content')

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">

            <div class="col-md-12 col-md-offset-0 admin-panel add-category" >
                <div class="panel panel-default">
                    <div class="panel-heading">products</div>

                    <div class="panel-body">
                        <table class="table table-striped tanksTable">
                            <thead ng-if="data.length > 0">
                                <tr> 
                                    <th>SKU</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Weight</th>
                                    <th>Website</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($products == null || count($products) <= 0 )
                                    <tr>
                                        <td title="No products found" >No products found</td>
                                        <td title="No products found" >  </td>
                                        <td title="No products found" >  </td>
                                        <td title="No products found" >  </td>
                                        <td title="No products found" >  </td>
                                    </tr>
                                @else
                                   @foreach($products as $product)
                                   <?php $product =  (array) $product; ?>
                                    <tr>         
                                        <td title="{{$product['sku']}}" ><span>{{$product['sku']}}</span></td>
                                        <td title="{{$product['name']}}" ><span>{{$product['name']}}</span></td>
                                        <td title="{{$product['unit_price']}}" ><span>${{$product['unit_price']}}</span></td>
                                        <td title="{{$product['weight']}}" ><span>{{$product['weight']}}</span></td>  
                                        <td title="{{$websiteList[$product['website_id']]}}" ><span>{{$websiteList[$product['website_id']]}}</span></td>                              
                                    </tr>  
                                    @endforeach  
                                    {{  $products->render() }}
                                @endif
                            </tbody> 
                        </table>
                    </div>
                </div>
            </div>
    
    </div>
</div>

@endsection