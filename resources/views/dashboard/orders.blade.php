@extends('layouts.app')

@section('scripts')

<link href="{{asset('template/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<script src="{{asset('template/js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    jQuery(document).ready(function($){
    	$("#add-order-details").click(function(){
    		var orderDetails = $(".order-details-section").first().clone(true);
    		$(".order-details-section").last().after(orderDetails);
    	})

    	$(".remove-order-detail").click(function(){
    		var element = $(this).parents(".order-details-section");
    		if($(".order-details-section").length > 1)
    		{
    			element.remove();
    		}
    	})

    	$("#show-shipping").change(function() {
		    if(this.checked) {
		        $("#shipping-address-field").show();
		    }
		    else
		    	$("#shipping-address-field").hide();
		});
		$('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
            cursor: true,
        });

        $('input').on('ifToggled', function(event){
		 	console.log(event);
		 	if(event.target.id == "show-shipping")
		 	{
		 		if($("#show-shipping").is(":checked"))
		 		{
	 				$("#shipping-address-field").show();
			    }
			    else
			    {
			    	$("#shipping-address-field").hide();
			 	}
			}
		});
    });
</script>

@endsection

@section('content')

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
   				
			@if (Session::has('error'))
               <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

             @if (Session::has('success'))
               <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif

	       	<div class="col-md-12 col-md-offset-0 admin-panel add-category">

				

	            <div class="panel panel-default">
	                <div class="panel-heading">Orders</div>

	                <div class="panel-body">
	                    <table class="table table-striped tanksTable">
	                        <thead ng-if="data.length > 0">
	                            <tr>   
	                                <th>Order id</th>
	                                <th>Order date</th>
	                                <th>Website</th>
	                                <th>Status</th>
	                                <th>Discount</th>
	                                <th>Order Total</th>	                                
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	@if($orders == null || count($orders) <= 0 )
	                        		<tr>
	                        			<td title="No orders found" >No orders found</td>
	                        			<td title="No orders found" >  </td>
	                        			<td title="No orders found" >  </td>
	                        			<td title="No orders found" >  </td>
	                        		</tr>
	                        	@else
		                           @foreach($orders as $order)
		                           <?php $order =  (array) $order; ?>
		                           
			                            <tr>         
			                                <td title="{{$order['id']}}" ><a href="order/{{$order['id']}}/edit">{{$order['id']}}</a></td>
			                                <td title="{{$order['order_date']}}" ><a href="order/{{$order['id']}}/edit">{{date('m/d/Y', strtotime($order['order_date']))}}</a></td>
			                                <td title="{{$websiteList[$order['website_id']]}}" ><a href="order/{{$order['id']}}/edit">{{$websiteList[$order['website_id']]}}</a></td>
			                                <td title="{{$order['status']}}" ><a href="order/{{$order['id']}}/edit">{{$order['status']}}</a></td>	
			                                <td title="{{$order['discount']}}" ><a href="order/{{$order['id']}}/edit">${{$order['discount']}}</a></td>	
			                                <td title="{{$order['total_charged']}}" ><a href="order/{{$order['id']}}/edit">${{$order['total_charged']}}</a></td>	                                
			                            </tr> 
		                           
		                            @endforeach  
		                            {{--	$orders->render() --}}
		                        @endif
	                        </tbody> 
	                    </table>

	                </div>
	            </div>
	        </div>
    
    </div>
</div>

@endsection