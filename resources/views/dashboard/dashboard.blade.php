@extends('layouts.app')

@section('content')
<!-- <div class="panel panel-default" style="margin-top:20px">
                
    <div class="panel-heading">Add customer</div>                    
    <div class="panel-body">
        {{ Form::open(['url' => '/customer', 'class'=>'m-t col-xs-12', 'method'=>'POST', 'role'=>'form', 'enctype'=>"multipart/form-data"]) }}

        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            {{ Form::text('first_name','',['class'=>'form-control', 'placeholder'=>'First name', 'required' => 'required']) }}
            @if ($errors->has('first_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
            {{ Form::text('last_name','',['class'=>'form-control', 'placeholder'=>'Last name', 'required' => 'required']) }}
            @if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('address_1') ? ' has-error' : '' }}">
            {{ Form::text('address_1','',['class'=>'form-control', 'placeholder'=>'Address', 'required' => 'required']) }}
            @if ($errors->has('address_1'))
                <span class="help-block">
                    <strong>{{ $errors->first('address_1') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
            {{ Form::text('city','',['class'=>'form-control', 'placeholder'=>'City', 'required' => 'required']) }}
            @if ($errors->has('city'))
                <span class="help-block">
                    <strong>{{ $errors->first('city') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
            {{ Form::text('state','',['class'=>'form-control', 'placeholder'=>'State', 'required' => 'required']) }}
            @if ($errors->has('state'))
                <span class="help-block">
                    <strong>{{ $errors->first('state') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
            {{ Form::text('country','',['class'=>'form-control', 'placeholder'=>'Country', 'required' => 'required']) }}
            @if ($errors->has('country'))
                <span class="help-block">
                    <strong>{{ $errors->first('country') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
            {{ Form::text('zip','',['class'=>'form-control', 'placeholder'=>'Zip', 'required' => 'required']) }}
            @if ($errors->has('zip'))
                <span class="help-block">
                    <strong>{{ $errors->first('zip') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
            {{ Form::text('phone','',['class'=>'form-control', 'placeholder'=>'Phone', 'required' => 'required']) }}
            @if ($errors->has('phone'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            {{ Form::email('email','',['class'=>'form-control', 'placeholder'=>'Email', 'required' => 'required']) }}
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">
            {{ Form::textarea('comments','',['class'=>'form-control', 'placeholder'=>'Comments']) }}
            @if ($errors->has('comments'))
                <span class="help-block">
                    <strong>{{ $errors->first('comments') }}</strong>
                </span>
            @endif
        </div>

       

        <div class="form-group fr">
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
        </div>

        {{Form::close()}}
    </div>
</div> -->

<!-- <div class="panel panel-default" style="margin-top:20px">
                
    <div class="panel-heading">Add Order</div>                    
    <div class="panel-body">
        {{ Form::open(['url' => '/order', 'class'=>'m-t col-xs-12', 'method'=>'POST', 'role'=>'form', 'enctype'=>"multipart/form-data"]) }}

       <div class="form-group{{ $errors->has('subtotal') ? ' has-error' : '' }}">
            {{ Form::text('subtotal','',['class'=>'form-control', 'placeholder'=>'Subtotal', 'required' => 'required']) }}
            @if ($errors->has('subtotal'))
                <span class="help-block">
                    <strong>{{ $errors->first('subtotal') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('shipping_cost') ? ' has-error' : '' }}">
            {{ Form::text('shipping_cost','',['class'=>'form-control', 'placeholder'=>'Shipping Cost', 'required' => 'required']) }}
            @if ($errors->has('shipping_cost'))
                <span class="help-block">
                    <strong>{{ $errors->first('shipping_cost') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
            {{ Form::text('discount','',['class'=>'form-control', 'placeholder'=>'Discount', 'required' => 'required']) }}
            @if ($errors->has('discount'))
                <span class="help-block">
                    <strong>{{ $errors->first('discount') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('tax') ? ' has-error' : '' }}">
            {{ Form::text('tax','',['class'=>'form-control', 'placeholder'=>'Tax', 'required' => 'required']) }}
            @if ($errors->has('tax'))
                <span class="help-block">
                    <strong>{{ $errors->first('tax') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('total_charged') ? ' has-error' : '' }}">
            {{ Form::text('total_charged','',['class'=>'form-control', 'placeholder'=>'Total Charged', 'required' => 'required']) }}
            @if ($errors->has('total_charged'))
                <span class="help-block">
                    <strong>{{ $errors->first('total_charged') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('credit_card') ? ' has-error' : '' }}">
            {{ Form::text('credit_card','',['class'=>'form-control', 'placeholder'=>'Credit card']) }}
            @if ($errors->has('credit_card'))
                <span class="help-block">
                    <strong>{{ $errors->first('credit_card') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('approval_code') ? ' has-error' : '' }}">
            {{ Form::text('approval_code','',['class'=>'form-control', 'placeholder'=>'Approval Code']) }}
            @if ($errors->has('approval_code'))
                <span class="help-block">
                    <strong>{{ $errors->first('approval_code') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('website_id') ? ' has-error' : '' }}">
            {{ Form::select('website_id',$websiteList,null, ['class' => 'form-control', 'placeholder' => 'Select website',  'required' => 'required']) }}
            @if ($errors->has('website_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('website_id') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
            {{ Form::select('status',[' completed' => ' completed', 'pending' => 'pending', 'processing' => 'processing','failed' => 'failed'],null, ['class' => 'form-control', 'placeholder' => 'Select status',  'required' => 'required']) }}
            @if ($errors->has('status'))
                <span class="help-block">
                    <strong>{{ $errors->first('status') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group fr">
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
        </div>

        {{Form::close()}}
    </div>
</div>

<div class="panel panel-default" style="margin-top:20px">
                
    <div class="panel-heading">Add Order details</div>                    
    <div class="panel-body">
        {{ Form::open(['url' => '/order-details', 'class'=>'m-t col-xs-12', 'method'=>'POST', 'role'=>'form', 'enctype'=>"multipart/form-data"]) }}

        <div class="form-group{{ $errors->has('sku') ? ' has-error' : '' }}">
            {{ Form::text('sku','',['class'=>'form-control', 'placeholder'=>'SKU', 'required' => 'required']) }}
            @if ($errors->has('sku'))
                <span class="help-block">
                    <strong>{{ $errors->first('sku') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
            {{ Form::number('quantity','',['class'=>'form-control', 'placeholder'=>'Quantity', 'required' => 'required']) }}
            @if ($errors->has('quantity'))
                <span class="help-block">
                    <strong>{{ $errors->first('quantity') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('unit_price') ? ' has-error' : '' }}">
            {{ Form::text('unit_price','',['class'=>'form-control', 'placeholder'=>'Unit Price', 'required' => 'required']) }}
            @if ($errors->has('unit_price'))
                <span class="help-block">
                    <strong>{{ $errors->first('unit_price') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
            {{ Form::text('discount','',['class'=>'form-control', 'placeholder'=>'Discount', 'required' => 'required']) }}
            @if ($errors->has('discount'))
                <span class="help-block">
                    <strong>{{ $errors->first('discount') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('order_id') ? ' has-error' : '' }}">
            {{ Form::number('order_id','',['class'=>'form-control', 'placeholder'=>'Order id (the order details will be assigned to)', 'required' => 'required']) }}
            @if ($errors->has('order_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('order_id') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('website_id') ? ' has-error' : '' }}">
            {{ Form::select('website_id',$websiteList,null, ['class' => 'form-control', 'placeholder' => 'Select website',  'required' => 'required']) }}
            @if ($errors->has('website_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('website_id') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group fr">
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
        </div>

        {{Form::close()}}
    </div>
</div> -->
@endsection
