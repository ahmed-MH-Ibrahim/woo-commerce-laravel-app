@extends('layouts.app')

@section('scripts')
<script>
$(document).ready(function(){
	$("#customer-search").click(function(e){

		$("#loading").show();
		e.preventDefault();
		e.stopPropagation();
		var link = $("#search-customer-link").text();
		var firstname = $(this).parents(".search-form").find("input[name='first_name']").val();
		var lastname = $(this).parents(".search-form").find("input[name='last_name']").val();
		var phone = $(this).parents(".search-form").find("input[name='phone']").val();
		var email = $(this).parents(".search-form").find("input[name='email']").val();
		var city = $(this).parents(".search-form").find("input[name='city']").val();

		$.get(link,{firstname:firstname,lastname:lastname,phone:phone,email:email,city:city},function(data){
			$("#loading").hide();
			$(".search-results table tbody").empty();
			$(".search-results").show();
			$(".customersTable").hide();

			if(data != null && data['error'] == null)
			{	
				if(data.length == 0)
				{
					$(".search-results table tbody").append("<tr>"         
	                    +"<td >No results found</td>"
	                    +"<td ></td>"
	                    +"<td ></td>"
	                    +"<td ></td>"
	                    +"<td ></td>"                                
	                +"</tr>");

	                return;
				}	

				for(key in data)
				{
					row = data[key];
					$(".search-results table tbody").append("<tr>"         
	                    +"<td ><a href=\"customer/"+row['id']+"/edit\">"+row['first_name']+" "+row['last_name']+"</a></td>"
	                    +"<td ><a href=\"customer/"+row['id']+"/edit\">"+row['email']+"</a></td>"
	                    +"<td ><a href=\"customer/"+row['id']+"/edit\">"+row['phone']+"</a></td>"
	                    +"<td ><a href=\"customer/"+row['id']+"/edit\">"+row['city']+"</a></td>"	   
	                    +"<td ><a href=\"customer/"+row['id']+"/edit\">"+row['country']+"</a></td>"	                                
	                +"</tr>");
				}
			}
		})
	})

	$('body').keypress(function (e) {
		var key = e.which;
		if(key == 13)  // the enter key code
		{
			$("#customer-search").trigger("click");
			return false;  
		}
	});  
})
</script>
@endsection

@section('content')
<div class="meta-data" id="search-customer-link" style="display:none;">{{url("/search-customers")}}</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">

    		@if (Session::has('error'))
               <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

             @if (Session::has('success'))
               <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif

	       	<div class="col-md-12 col-md-offset-0 admin-panel add-category" >

	       		<div class="add-customer-container">
	       			<a href="{{url('/customer/create')}}" class="btn btn-primary fr">Add Customer</a>
	       		</div>


	            <div class="panel panel-default">
	                <div class="panel-heading">Customers</div>

	                <div class="panel-body">

	                	{{ Form::open(['url' => '/search-customers', 'class'=>' col-xs-12 search-form', 'method'=>'Get', 'role'=>'form', 'enctype'=>"multipart/form-data"]) }}

				        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} col-xs-6">
				            {{ Form::text('first_name','',['class'=>'form-control', 'placeholder'=>'Search by first name']) }}
				            @if ($errors->has('first_name'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('first_name') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} col-xs-6">
				            {{ Form::text('last_name','',['class'=>'form-control', 'placeholder'=>'Search by last name']) }}
				            @if ($errors->has('last_name'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('last_name') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-xs-4">
				            {{ Form::text('email','',['class'=>'form-control', 'placeholder'=>'Search by email']) }}
				            @if ($errors->has('email'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('email') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} col-xs-4">
				            {{ Form::text('phone','',['class'=>'form-control', 'placeholder'=>'Search by phone']) }}
				            @if ($errors->has('phone'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('phone') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }} col-xs-4">
				            {{ Form::text('city','',['class'=>'form-control', 'placeholder'=>'Search by city']) }}
				            @if ($errors->has('city'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('city') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group fr">
				            {{ Form::submit('Search', ['class' => 'btn btn-primary', 'id' => 'customer-search']) }}
				        </div>

				        {{Form::close()}}

				        <div class="search-results">
				        	<table class="table table-striped">
		                        <thead ng-if="data.length > 0">
		                            <tr>   
		                                <th>Name</th>
		                                <th>Email</th>
		                                <th>Phone</th>
		                                <th>City</th>
		                                <th>Country</th>
		                            </tr>
		                        </thead>
		                        <tbody>	                    
		                            
		                        </tbody> 
		                    </table>
				        </div>

	                    <table class="table table-striped customersTable">
	                        <thead ng-if="data.length > 0">
	                            <tr>   
	                                <th>Name</th>
	                                <th>Email</th>
	                                <th>Phone</th>
	                                <th>City</th>
	                                <th>Country</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	@if($customers == null || count($customers) <= 0 )
	                        		<tr>
	                        			<td title="No customers found" >No customers found</td>
	                        			<td title="No customers found" >  </td>
	                        			<td title="No customers found" >  </td>
	                        			<td title="No customers found" >  </td>
	                        		</tr>
	                        	@else
		                           @foreach($customers as $customer)
		                           <?php $customer =  (array) $customer; ?>
		                            <tr>         
		                                <td title="{{$customer['first_name'].''.$customer['last_name']}}" ><a href="customer/{{$customer['id']}}/edit">{{$customer['first_name'].' '.$customer['last_name']}}</a></td>
		                                <td title="{{$customer['email']}}" ><a href="customer/{{$customer['id']}}/edit">{{$customer['email']}}</a></td>
		                                <td title="{{$customer['phone']}}" ><a href="customer/{{$customer['id']}}/edit">{{$customer['phone']}}</a></td>
		                                <td title="{{$customer['city']}}" ><a href="customer/{{$customer['id']}}/edit">{{$customer['city']}}</a></td>	                                
		                                <td title="{{$customer['country']}}" ><a href="customer/{{$customer['id']}}/edit">{{$customer['country']}}</a></td>	                                
		                            </tr>  
		                            @endforeach  
		                            {{--	$customers->render() --}}
		                        @endif
	                        </tbody> 
	                    </table>
	                </div>
	            </div>
	        </div>
    
    </div>
</div>

@endsection