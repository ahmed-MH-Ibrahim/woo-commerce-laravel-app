@extends('layouts.app')

@section('scripts')

<link href="{{asset('template/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<script src="{{asset('template/js/plugins/iCheck/icheck.min.js')}}"></script>
<script>

	var availableTags = <?php echo json_encode($productJSList); ?>;
	var availableSKUs = <?php echo json_encode($skuJSList); ?>;
	
	var site_shipping_type = <?php echo json_encode($site_shipping_type); ?>;

    jQuery(document).ready(function($){

    	availableTags = unique(availableTags);
    	availableSKUs = unique(availableSKUs);

    	calculateTotal();

    	$(".sku-field").each(function(){
    		$(this).change(function(){
				var element = $(this).parents("tr");
				//var id = $(this).val();
				var text = $(this).val();//$(this).children(':selected').text();
				var lookupRow = $("#products-lookup div[data-sku='"+text+"']").first();

				element.find("td.unitprice input").val(lookupRow.find(".unit_price").text());
				element.find("td.productname input").val(lookupRow.find(".name").text());

				updateOrderDetailsTotal($(this));
			})
    	})

		$(".productname-field").each(function(){
			$(this).change(function(){
				var element = $(this).parents("tr");
				//var id = $(this).val();
				var text = $(this).val();//$(this).children(':selected').text();
				var lookupRow = $("#products-lookup div[data-pname='"+text+"']").first();

				element.find("td.unitprice input").val(lookupRow.find(".unit_price").text());
				element.find("td.sku input").val(lookupRow.find(".sku").text());
				updateOrderDetailsTotal($(this));
			})
		});

		$(".productname-field").each(function(){
			$(this).autocomplete({
		    	source: availableTags
		    });
		});

		$(".sku-field").each(function(){
    		$(this).autocomplete({
		    	source: availableSKUs
		    });
    	})


		$("#show-shipping").change(function() {
		    if(this.checked) {
		        $(".shipping-field").show();
		    }
		    else
		    	$(".shipping-field").hide();

		    //checkShipping();
		});

		$("input[name='shipping_country']").change(function(){
			if($("#show-shipping").is(":checked"))
			{
				checkShipping();
			}
		})

		$("#website_id").change(function(){
			checkShipping();
		})

    	$("#add-order-details").click(function(){
    		var text = $(".order-details-section table tr").first().find("td.sku select").val();
    		var orderDetails = $(".order-details-section table tr").first().clone(false);

    		
    		$(".order-details-section table tr").last().after(orderDetails);

    		var addedItem = $(".order-details-section table tr").last();
    		addedItem.find("td.sku input").attr("name","sku[]").val("");
    		addedItem.find("td.unittotal input").attr("name","order-detail-total[]").val("");
    		addedItem.find("td.discount input").attr("name","discount[]").attr("value","0");
    		addedItem.find("td.unitprice input").attr("name","unit_price[]").val("");
    		addedItem.find("td.quantity input").attr("name","quantity[]").attr("value","1");
    		addedItem.find("td.productname input").attr("name","productName[]").val("");

    		addedItem.find("td.sku select").val(text).change();

    		$(".order-details-section table tr").last().find(".productname-field").last().autocomplete({
			    	source: availableTags
			});

			$(".order-details-section table tr").last().find("#sku-field").last().autocomplete({
			    	source: availableSKUs
			});

    		$(".order-details-section table tr").last().find("#sku-field").change(function(){
				var element = $(this).parents("tr");
				//var id = $(this).val();
				var text = $(this).val();//$(this).children(':selected').text();
				var lookupRow = $("#products-lookup div[data-sku='"+text+"']").first();;

				element.find("td.unitprice input").val(lookupRow.find(".unit_price").text());
				element.find("td.productname input").val(lookupRow.find(".name").text());

				updateOrderDetailsTotal($(this));
			})

			$(".order-details-section table tr").last().find("#productname-field").last().change(function(){
				var element = $(this).parents("tr");
				//var id = $(this).val();
				var text = $(this).val();//$(this).children(':selected').text();
				var lookupRow = $("#products-lookup div[data-pname='"+text+"']").first();;

				element.find("td.unitprice input").val(lookupRow.find(".unit_price").text());
				element.find("td.sku input").val(lookupRow.find(".sku").text());
				updateOrderDetailsTotal($(this));
			})

			$(".order-details-section table tr").last().find(".remove-order-detail").last().click(function(){
	    		var element = $(this).parents(".orderdetails-row");
	    		if($(".orderdetails-row").length > 1)
	    		{
	    			element.remove();
	    			calculateTotal();
	    			checkShipping();
	    		}
	    	})

	    	$(".order-details-section table tr").last().find(".quantity input, .discount input").change(function(){
				updateOrderDetailsTotal($(this));
			})

    		/*$(".productname-field").each(function(){
				$(this).autocomplete({
			    	source: availableTags
			    });
			});*/

    	})

    	$(".remove-order-detail").click(function(){
    		var element = $(this).parents(".orderdetails-row");
    		if($(".orderdetails-row").length > 1)
    		{
    			element.remove();
    			calculateTotal();
    			checkShipping();
    		}
    	})

    	
		$('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
            cursor: true,
        });

        $('input').on('ifToggled', function(event){
		 	console.log(event);
		 	if(event.target.id == "show-shipping")
		 	{
		 		if($("#show-shipping").is(":checked"))
		 		{
	 				$(".shipping-field").show();
			    }
			    else
			    {
			    	$(".shipping-field").hide();
			 	}

			 	checkShipping();
			}
		});

		if($("#show-shipping").is(":checked"))
 		{
			$(".shipping-field").show();
	    }
	    else
	    {
	    	$(".shipping-field").hide();
	 	}

/*		$('#credit-expiration-field').datepicker( {
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'mm/yy',
	        onClose: function(dateText, inst) { 
	            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
	        }
	    });*/
		
    });
</script>

@endsection

@section('content')

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">

    		@if (Session::has('error'))
               <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

             @if (Session::has('success'))
               <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif

	       	<div class="col-md-12 col-md-offset-0 admin-panel add-category">
	       		{{ Form::open(['url' => '/order', 'class'=>'','style' => 'overflow:hidden', 'method'=>'POST', 'role'=>'form', 'enctype'=>"multipart/form-data"]) }}
	       		<div class="panel panel-default" style="">
                
				    <div class="panel-heading">New order for {{$customer->first_name}} {{$customer->last_name}}</div>                    
				    <div class="panel-body">

				        <div class="form-group{{ $errors->has('website_id') ? ' has-error' : '' }}  col-xs-6 websites-dropdown">
				            {{ Form::select('website_id',$websiteList,null, ['class' => 'form-control', 'placeholder' => 'Select website', 'id' =>'website_id', 'required' => 'required']) }}
				            @if ($errors->has('website_id'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('website_id') }}</strong>
				                </span>
				            @endif
				        </div>				       

				        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}  col-xs-6">
				            {{ Form::select('status',config("websites.woocommerce_order_statuses_list"),'processing', ['class' => 'form-control', 'placeholder' => 'Select status',  'required' => 'required']) }}
				            @if ($errors->has('status'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('status') }}</strong>
				                </span>
				            @endif
				        </div>				        

				        <div class="form-group col-xs-12">

				        		
				        		<label> {{Form::checkbox('show-shipping', 'enabled', false,[ 'id' => 'show-shipping' ,'class' => 'i-checks']) }} Add shipping details</label>
				        	
				        </div>

				        <div class="form-group{{ $errors->has('shipping_first_name') ? ' has-error' : '' }} col-xs-6 shipping-field">
				            {{ Form::text('shipping_first_name','',['class'=>'form-control', 'placeholder'=>'First name']) }}
				            @if ($errors->has('shipping_first_name'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('shipping_first_name') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('shipping_last_name') ? ' has-error' : '' }} col-xs-6 shipping-field">
				            {{ Form::text('shipping_last_name','',['class'=>'form-control', 'placeholder'=>'Last name']) }}
				            @if ($errors->has('shipping_last_name'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('shipping_last_name') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('shipping_address_1') ? ' has-error' : '' }} col-xs-6 shipping-field" >
				            {{ Form::text('shipping_address_1','',['class'=>'form-control', 'placeholder'=>'Shipping Address 1']) }}
				            @if ($errors->has('shipping_address_1'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('shipping_address_1') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('shipping_address_2') ? ' has-error' : '' }} col-xs-6 shipping-field" >
				            {{ Form::text('shipping_address_2','',['class'=>'form-control', 'placeholder'=>'Shipping Address 2']) }}
				            @if ($errors->has('shipping_address_2'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('shipping_address_2') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('shipping_city') ? ' has-error' : '' }} col-sm-3 col-xs-6 shipping-field" >
				            {{ Form::text('shipping_city','',['class'=>'form-control', 'placeholder'=>'City']) }}
				            @if ($errors->has('shipping_city'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('shipping_city') }}</strong>
				                </span>
				            @endif
				        </div>
				        <div class="form-group{{ $errors->has('shipping_state') ? ' has-error' : '' }} col-sm-3 col-xs-6 shipping-field" >
				            {{ Form::text('shipping_state','',['class'=>'form-control', 'placeholder'=>'State']) }}
				            @if ($errors->has('shipping_state'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('shipping_state') }}</strong>
				                </span>
				            @endif
				        </div>

				         <div class="form-group{{ $errors->has('shipping_postcode') ? ' has-error' : '' }} col-sm-3 col-xs-6 shipping-field" >
				            {{ Form::text('shipping_postcode','',['class'=>'form-control', 'placeholder'=>'Zip']) }}
				            @if ($errors->has('shipping_postcode'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('shipping_address_1') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('shipping_country') ? ' has-error' : '' }} col-sm-3 col-xs-6 shipping-field" >
				            {{ Form::text('shipping_country','US',['class'=>'form-control', 'placeholder'=>'Country']) }}
				            @if ($errors->has('shipping_country'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('shipping_country') }}</strong>
				                </span>
				            @endif
				        </div>

				       				      

				        
				    </div>
				</div>

				<input id="nonce" name="payment_method_nonce" type="hidden" />


				<div class="panel panel-default order-details-section" style="margin-top:20px">
				                
				    <div class="panel-heading" style="overflow:hidden">New Order details</div>                    
				    <div class="panel-body">
				        {{-- Form::open(['url' => '/order-details', 'class'=>'m-t col-xs-12', 'method'=>'POST', 'role'=>'form', 'enctype'=>"multipart/form-data"]) --}}

				        <table class="col-xs-12">
				        	<tbody>
			        		@if(old('sku'))
					            <?php $i=0;?>
					            @foreach (old('sku') as $key => $sku)
				        		<tr class="orderdetails-row">
							        <td class="form-group{{ $errors->has('sku.$i') ? ' has-error' : '' }} sku">	
				        						<?php $sku = old('sku')[$key]; ?>		            
							            	{{ Form::text("sku[$i]",$sku, ['class' => 'form-control sku-field', 'placeholder' => 'SKU', 'id' =>'sku-field',  'required' => 'required']) }}
								            @if ($errors->has("sku.$i"))
								                <span class="help-block">
								                    <strong>{{ $errors->first("sku.$i") }}</strong>
								                </span>
								            @endif
							            

								      
							        </td>

							        <td class="form-group productname">

							        		<?php $productName = old('productName')[$key]; ?>
							            	{{ Form::text("productName[$i]",$productName, ['class' => 'form-control productname-field', 'placeholder' => 'Product name', 'id' =>'productname-field']) }}
								            @if ($errors->has("productName[$i]"))
								                <span class="help-block">
								                    <strong>{{ $errors->first("productName[$i]") }}</strong>
								                </span>
								            @endif


								     
							        </td>

							        <td class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }} quantity">
	
							        		<?php $quantity = old('quantity')[$key]; ?>
								            	{{ Form::number("quantity[$i]",$quantity, ['class'=>'form-control', 'placeholder'=>'Quantity', 'required' => 'required']) }}									            
									            @if ($errors->has("quantity[$i]"))
									                <span class="help-block">
									                    <strong>{{ $errors->first("quantity[$i]") }}</strong>
									                </span>
									            @endif
								    
								     

							        </td>

							        <td class="form-group{{ $errors->has('unit_price') ? ' has-error' : '' }} unitprice" >
				
							        		<?php $unit_price = old('unit_price')[$key]; ?>
								            	{{ Form::text("unit_price[$i]",$unit_price,['class'=>'form-control', 'placeholder'=>'Unit Price', 'required' => 'required', 'readonly' => 'readonly']) }}
								            	
									            @if ($errors->has("unit_price[$i]"))
									                <span class="help-block">
									                    <strong>{{ $errors->first("unit_price[$i]") }}</strong>
									                </span>
									            @endif

						
								      

							            
							        </td>

							        <td class="form-group{{ $errors->has('discount') ? ' has-error' : '' }} discount">
							        	 
							    
							        		<?php $discount = old('discount')[$key]; ?>
								            	{{ Form::text("discount[$i]",$discount,['class'=>'form-control', 'placeholder'=>'Discount %', 'required' => 'required']) }}
								            	
									            @if ($errors->has("discount[$i]"))
									                <span class="help-block">
									                    <strong>{{ $errors->first("discount[$i]") }}</strong>
									                </span>
									            @endif

								     
								   
							            
							        </td>
							        <td class="form-group unittotal">
							        
							        		<?php $orderdetailtotal = old('order-detail-total')[$key]; ?>
								            	{{ Form::text("order-detail-total[$i]",$orderdetailtotal,['class'=>'form-control order-details-total-field', 'placeholder'=>'Total', 'required' => 'required', "readonly" => 'readonly']) }}
								            	
									            @if ($errors->has("order-detail-total[$i]"))
									                <span class="help-block">
									                    <strong>{{ $errors->first("order-detail-total[$i]") }}</strong>
									                </span>
									            @endif

	
							        </td>
							        <td class="remove-order-detail" title="remove">x</td>
							    </tr>
						       <?php $i++;?>
						       @endforeach
						    @else
						    	<tr class="orderdetails-row">
							        <td class="form-group{{ $errors->has('sku.$i') ? ' has-error' : '' }} sku">	
				        	
							            

								        	{{ Form::text('sku[]','', ['class' => 'form-control sku-field', 'placeholder' => 'SKU', 'id' =>'sku-field',  'required' => 'required']) }}

							        </td>

							        <td class="form-group productname">


								        	{{ Form::text('productName[]','', ['class' => 'form-control productname-field','id' =>'productname-field', 'placeholder' => 'Product name']) }}
					
							            
							        </td>

							        <td class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }} quantity">
	
							     
								    
								        	{{ Form::number('quantity[]','1',['class'=>'form-control', 'placeholder'=>'Quantity', 'required' => 'required']) }}
							 

							        </td>

							        <td class="form-group{{ $errors->has('unit_price') ? ' has-error' : '' }} unitprice" >
				
							        

						
								        	{{ Form::text('unit_price[]','',['class'=>'form-control', 'placeholder'=>'Unit Price', 'required' => 'required', 'readonly' => 'readonly']) }}
							          

							            
							        </td>

							        <td class="form-group{{ $errors->has('discount') ? ' has-error' : '' }} discount">
							        	 
							    

								     
								        	{{ Form::text('discount[]','0',['class'=>'form-control', 'placeholder'=>'Discount %', 'required' => 'required']) }}
							           

							            
							        </td>
							        <td class="form-group unittotal">							        
								   
								        	{{ Form::text('order-detail-total[]','',['class'=>'form-control order-details-total-field', 'placeholder'=>'Total', 'required' => 'required', "readonly" => 'readonly']) }}
							         							        	
							        </td>
							        <td class="remove-order-detail" title="remove">x</td>
							    </tr>
							@endif
					        </tbody>
					    </table>
				    </div>
				</div>

				<div class="" style="overflow:hidden;margin-bottom:20px;" ><span class="btn btn-primary fr" id="add-order-details">Add order detail</span></div>


				<div class="order-summary col-sm-6 col-xs-12">
			        <div class="item{{ $errors->has('subtotal') ? ' has-error' : '' }}">
			            <div class="col col-xs-6">Subtotal</div>
			            <div class="value col-xs-6 order-subtotal">{{ Form::text('subtotal','0.00',['class'=>'col-xs-12', 'placeholder'=>'Subtotal', 'disabled' => 'disabled','required' => 'required']) }}
			            	@if ($errors->has('subtotal'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('subtotal') }}</strong>
				                </span>
				            @endif
				        </div>
			        </div>

			         <div class="item{{ $errors->has('shipping_method') ? ' has-error' : '' }} ">
			         	<div class="col col-xs-6">Shipping method</div>
			            <div class="value col-xs-6 order-shipping-method">{{ Form::select('shipping_method',config('websites.shipping_method'),'normal', ['class' => 'col-xs-12', 'placeholder' => 'Shipping method',  'required' => 'required']) }}
				            @if ($errors->has('shipping_method'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('shipping_method') }}</strong>
				                </span>
				            @endif
				        </div>
			        </div>

			        <div class="item{{ $errors->has('shipping_cost') ? ' has-error' : '' }} ">
			            <div class="col col-xs-6">Shipping cost</div>
			            <div class="value col-xs-6 order-shipping">{{ Form::text('shipping_cost','0.00',['class'=>'col-xs-12',  'placeholder'=>'Shipping Cost', 'required' => 'required']) }}
				            @if ($errors->has('shipping_cost'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('shipping_cost') }}</strong>
				                </span>
				            @endif
				        </div>
			        </div>

			        <div class="item{{ $errors->has('total_discount') ? ' has-error' : '' }} ">
			            <div class="col col-xs-6">Discount</div>
			            <div class="value col-xs-6 order-discount">{{ Form::text('total_discount','0.00',['class'=>'col-xs-12', 'placeholder'=>'Discount', 'required' => 'required']) }}
				            @if ($errors->has('total_discount'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('total_discount') }}</strong>
				                </span>
				            @endif
				        </div>
			        </div>
			        <div class="item{{ $errors->has('tax') ? ' has-error' : '' }} ">
			            <div class="col col-xs-6">Tax</div>
			            <div class="value col-xs-6 order-tax">{{ Form::text('tax','0.00',['class'=>'col-xs-12', 'placeholder'=>'Tax', 'required' => 'required']) }}
				            @if ($errors->has('tax'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('tax') }}</strong>
				                </span>
				            @endif
				        </div>
			        </div>
			        <hr/>
			        <div class="item{{ $errors->has('total_charged') ? ' has-error' : '' }} ">
			            <div class="col col-xs-6">Total</div>
			            <div class="value col-xs-6 order-total">{{ Form::text('total_charged','',['class'=>'col-xs-12', 'placeholder'=>'Total Charged', 'required' => 'required', 'disabled' => 'disabled']) }}
				            @if ($errors->has('total_charged'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('total_charged') }}</strong>
				                </span>
				            @endif
				        </div>
			        </div>  
			        <div class="form-group{{ $errors->has('customer-id') ? ' has-error' : '' }}  col-xs-6">
			        	<input type="hidden" name="customer-id" value="{{$customer->id}}">
			         	@if ($errors->has('customer-id'))
			                <span class="help-block">
			                    <strong>{{ $errors->first('customer-id') }}</strong>
			                </span>
			            @endif

			             <!-- other hidden fields -->
			        	<input type="hidden" name="customer-billing-country" id="customer-billing-country" value="{{$customer->country}}">  
			        </div>    

			               
			    </div>


			    <div class="panel panel-default col-xs-12" style="padding:0 !important">
                
                	<div class="panel-heading">Payments</div>                    
	                <div class="panel-body">	             

	                    <div class="form-group{{ $errors->has('credit_card') ? ' has-error' : '' }} col-sm-4 col-xs-6">
				            {{ Form::text('credit_card','',['class'=>'form-control', 'placeholder'=>'Credit card' , 'required' => 'required']) }}
				            @if ($errors->has('credit_card'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('credit_card') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('credit_card_exp') ? ' has-error' : '' }} col-sm-4 col-xs-6">
				            {{ Form::text('credit_card_exp','',['class'=>'form-control', 'maxlength' =>"4", 'placeholder'=>'Credit card expiration (MMYY)', 'id' => 'credit-expiration-field', 'required' => 'required']) }}
				            @if ($errors->has('credit_card_exp'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('credit_card_exp') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('credit_card_cvn') ? ' has-error' : '' }} col-sm-4 col-xs-6">
				            {{ Form::text('credit_card_cvn','',['class'=>'form-control', 'placeholder'=>'Credit card CVN', 'maxlength' => '4', 'required' => 'required']) }}
				            @if ($errors->has('credit_card_cvn'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('credit_card_cvn') }}</strong>
				                </span>
				            @endif
				        </div>

				        

	                    <div class="form-group{{ $errors->has('charge') ? ' has-error' : '' }} col-sm-4 col-xs-6 payment-charge">
	                        {{ Form::text('charge','',['class'=>'form-control', 'placeholder'=>'Amount to charge', 'required' => 'required']) }}
	                        @if ($errors->has('charge'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('charge') }}</strong>
	                            </span>
	                        @endif
	                    </div>


	                </div>
	            </div>

				<div class="form-group col-xs-12">
		            {{ Form::submit('Add order', ['class' => 'btn btn-primary fr']) }}
		        </div>



		        {{Form::close()}}

	        </div>
    
    </div>
</div>

<div id="products-lookup" style="display:none;">
	@foreach($products as $product)
    <div data-sku="{{$product['sku']}}" data-id="{{$product['id']}}" data-pname ="{{$product['name']}}" >   
        <span class="sku">{{$product['sku']}}</span>
        <span class="name">{{$product['name']}}</span>
        <span class="unit_price">{{$product['unit_price']}}</span>  
        <span class="weight">{{$product['weight']}}</span>  	                            
    </div>  
    @endforeach  
</div>

@endsection

