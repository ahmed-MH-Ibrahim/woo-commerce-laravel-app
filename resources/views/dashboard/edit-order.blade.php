@extends('layouts.app')

@section('content')


    <div class="row">

        <div class="col-md-12 col-md-offset-0 admin-panel add-category" style="margin-top:20px">
            @if (Session::has('success'))
               <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
               <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

            <div class="panel panel-default">
                
                <div class="panel-heading">Edit order</div>                    
                <div class="panel-body">
                    {{ Form::open(['url' => '/order/'.$id, 'class'=>' col-xs-12', 'method'=>'PUT', 'role'=>'form', 'enctype'=>"multipart/form-data"]) }}

                    <div class="form-group{{ $errors->has('id') ? ' has-error' : '' }} col-sm-6 col-xs-6">
                        <label>Order ID: </label>
                        <span>{{$order['id']}}</span>
                    </div>

                    <div class="form-group{{ $errors->has('order_date') ? ' has-error' : '' }} col-sm-6 col-xs-6">
                        <label>Order Date: </label>
                        <span>{{date('m/d/Y', strtotime($order['order_date']))}}</span>
                    </div>

                    <div class="form-group{{ $errors->has('website_id') ? ' has-error' : '' }} col-sm-6 col-xs-6 websites-dropdown">
                        <label>Website</label>
                        {{ Form::select('website_id',$websiteList,$order['website_id'], ['class' => 'form-control', 'placeholder' => 'Select website',  'required' => 'required']) }}
                        @if ($errors->has('website_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('website_id') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }} col-sm-6 col-xs-6">
                        <label>Status</label>
                        {{ Form::select('status',config("websites.woocommerce_order_statuses_list"),$order['status'], ['class' => 'form-control', 'placeholder' => 'Select status',  'required' => 'required']) }}
                        @if ($errors->has('status'))
                            <span class="help-block">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>

                   
                    @if($order["status"] != "completed")
                        <div class="form-group  col-xs-12">
                            {{ Form::submit('Save', ['class' => 'btn btn-primary fr']) }}
                        </div>
                    @endif

                    {{Form::close()}}
                </div>
            </div>

            <div class="panel panel-default">
                
                <div class="panel-heading">Payments</div>                    
                <div class="panel-body">
                    {{ Form::open(['url' => '/order/refund/'.$id, 'class'=>'', 'method'=>'POST', 'role'=>'form', 'enctype'=>"multipart/form-data"]) }}

                    <div class="form-group{{ $errors->has('refund') ? ' has-error' : '' }} col-xs-6">
                        <label>Refund amount</label>
                        {{ Form::text('refund',$order['refund'],['class'=>'form-control', 'placeholder'=>'Refund amount', 'required' => 'required']) }}
                        @if ($errors->has('refund'))
                            <span class="help-block">
                                <strong>{{ $errors->first('refund') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('approval_code') ? ' has-error' : '' }} col-xs-6">
                        <label>Approval Code</label>
                        {{ Form::text('approval_code',$order['approval_code'],['class'=>'form-control', 'placeholder'=>'Approval Code', 'disabled' => 'disabled']) }}
                        @if ($errors->has('approval_code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('approval_code') }}</strong>
                            </span>
                        @endif
                    </div>

                    @if($order["status"] != "completed")
                        <div class="form-group  ">
                            {{ Form::submit('Refund', ['class' => 'btn btn-primary fr']) }}
                        </div>
                    @endif

                    {{Form::close()}}

                    {{ Form::open(['url' => '/order/void/'.$id, 'class'=>'', 'method'=>'POST', 'role'=>'form', 'enctype'=>"multipart/form-data"]) }}

                    {{ Form::hidden('approval_code',$order['approval_code'],['class'=>'form-control', 'placeholder'=>'Approval Code', 'disabled' => 'disabled']) }}

                    @if($order["status"] != "completed")
                        <div class="form-group   ">

                            {{ Form::submit('Void transaction', ['class' => 'btn btn-primary fr', "style" => "margin-right:10px;"]) }}
                        </div>
                    @endif

                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Order Details</div>  
        <div class="panel-body">
            <table class="table table-striped tanksTable">
                <thead ng-if="data.length > 0">
                    <tr> 
                        <th>SKU</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Discount</th>  
                        <th>Total</th>                                
                    </tr>
                </thead>
                <tbody>
                    @if($order->details == null )
                        <tr>
                            <td title="No orders found" >No orders details found</td>
                            <td title="No orders found" >  </td>
                            <td title="No orders found" >  </td>
                            <td title="No orders found" >  </td>
                        </tr>
                    @else
                       @foreach($order->details as $orderDetail)
                        <tr>         
                            <td title="{{$orderDetail['sku']}}" ><span>{{$orderDetail['sku']}}</span></td>
                            <td title="{{$orderDetail['unit_price']}}" ><span>{{$productsList[$orderDetail['sku']]}}</span></td>
                            <td title="{{$orderDetail['quantity']}}" ><span>{{$orderDetail['quantity']}}</span></td>    
                            <td title="{{$orderDetail['unit_price']}}" ><span style="text-align:right;">${{number_format((float)$orderDetail['unit_price'], 2, '.', '')}}</span></td>
                            <td title="{{$orderDetail['discount']}}" ><span>{{$orderDetail['discount']}}%</span></td> 
                            <td title="{{$orderDetail['unit_price']}}" ><span style="text-align:right;">${{number_format((float)$orderDetail['quantity']*(float)$orderDetail['unit_price']*(float)(1-(float)$orderDetail["discount"]/(float)100), 2, '.', '') }}</span></td>                               
                        </tr> 
                        @endforeach  
                    @endif
                </tbody> 
            </table>

        </div>
    </div>

    <div class="order-summary col-sm-6 col-xs-12">
        <div class="item">
            <div class="col col-xs-6">Subtotal</div>
            <div class="value col-xs-6">${{number_format((float)$order['subtotal'], 2, '.', '')}}</div>
        </div>
        <div class="item">
            <div class="col col-xs-6">Shipping cost</div>
            <div class="value col-xs-6">${{number_format((float)$order['shipping_cost'], 2, '.', '')}}</div>
        </div>
        <div class="item">
            <div class="col col-xs-6">Discount</div>
            <div class="value col-xs-6">${{number_format((float)$order['discount'], 2, '.', '')}}</div>
        </div>
        <div class="item">
            <div class="col col-xs-6">Tax</div>
            <div class="value col-xs-6">${{number_format((float)$order['tax'], 2, '.', '')}}</div>
        </div>
        <hr/>
        <div class="item">
            <div class="col col-xs-6">Total</div>
            <div class="value col-xs-6">${{number_format((float)$order['total_charged'], 2, '.', '')}}</div>
        </div>                
    </div>


@endsection
