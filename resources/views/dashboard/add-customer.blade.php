@extends('layouts.app')

@section('content')
<div class="meta-data" id="search-customer-link" style="display:none;">{{url("/search-customers")}}</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">

    		@if (Session::has('error'))
               <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

             @if (Session::has('success'))
               <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            
	       	<div class="col-md-12 col-md-offset-0 admin-panel add-category" >
	       		<div class="panel panel-default" style="">
                
				    <div class="panel-heading">Add customer</div>                    
				    <div class="panel-body">
				        {{ Form::open(['url' => '/customer', 'class'=>'m-t col-xs-12', 'method'=>'POST', 'role'=>'form', 'enctype'=>"multipart/form-data"]) }}

				        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} col-xs-6">
				            {{ Form::text('first_name','',['class'=>'form-control', 'placeholder'=>'First name', 'required' => 'required']) }}
				            @if ($errors->has('first_name'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('first_name') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} col-xs-6">
				            {{ Form::text('last_name','',['class'=>'form-control', 'placeholder'=>'Last name', 'required' => 'required']) }}
				            @if ($errors->has('last_name'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('last_name') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('address_1') ? ' has-error' : '' }} col-xs-6">
				            {{ Form::text('address_1','',['class'=>'form-control', 'placeholder'=>'Address', 'required' => 'required']) }}
				            @if ($errors->has('address_1'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('address_1') }}</strong>
				                </span>
				            @endif
				        </div>

				         <div class="form-group{{ $errors->has('address_2') ? ' has-error' : '' }} col-xs-6">
				            {{ Form::text('address_2','',['class'=>'form-control', 'placeholder'=>'Address 2']) }}
				            @if ($errors->has('address_2'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('address_2') }}</strong>
				                </span>
				            @endif
				        </div>


				        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }} col-sm-3 col-xs-6">
				            {{ Form::text('city','',['class'=>'form-control', 'placeholder'=>'City', 'required' => 'required']) }}
				            @if ($errors->has('city'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('city') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }} col-sm-3 col-xs-6">
				            {{ Form::text('state','',['class'=>'form-control', 'placeholder'=>'State', 'required' => 'required']) }}
				            @if ($errors->has('state'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('state') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }} col-sm-3 col-xs-6">
				            {{ Form::text('zip','',['class'=>'form-control', 'placeholder'=>'Zip', 'required' => 'required']) }}
				            @if ($errors->has('zip'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('zip') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }} col-sm-3 col-xs-6">
				            {{ Form::text('country','US',['class'=>'form-control', 'placeholder'=>'Country', 'required' => 'required']) }}
				            @if ($errors->has('country'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('country') }}</strong>
				                </span>
				            @endif
				        </div>

				        

				        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} col-xs-6">
				            {{ Form::text('phone','',['class'=>'form-control', 'placeholder'=>'Phone', 'required' => 'required']) }}
				            @if ($errors->has('phone'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('phone') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-xs-6">
				            {{ Form::email('email','',['class'=>'form-control', 'placeholder'=>'Email', 'required' => 'required']) }}
				            @if ($errors->has('email'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('email') }}</strong>
				                </span>
				            @endif
				        </div>

				        <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }} col-xs-12">
				            {{ Form::textarea('comments','',['class'=>'form-control', 'placeholder'=>'Comments']) }}
				            @if ($errors->has('comments'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('comments') }}</strong>
				                </span>
				            @endif
				        </div>

				       

				        <div class="form-group fr">
				            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
				        </div>

				        {{Form::close()}}
				    </div>
				</div>


	        </div>
    
    </div>
</div>

@endsection