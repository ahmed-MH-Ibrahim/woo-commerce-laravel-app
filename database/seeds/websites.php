<?php

use Illuminate\Database\Seeder;

use App\Website;

class websites extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        $sites = config('websites.sites');

        foreach ($sites as $key => $value) {
        	$site = new Website();
        	$site->name = $value;
        	$site->short = $key;
        	$site->save();
        }
    }
}
