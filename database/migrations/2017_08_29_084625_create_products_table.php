<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('website_id')->unsigned();
            //$table->string('product_id')->nullable();
            $table->string('sku');
            $table->string('name');
            $table->string('unit_price');
            $table->string('weight');
            $table->timestamps();

            $table->foreign('website_id')
                  ->references('id')->on('websites')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
