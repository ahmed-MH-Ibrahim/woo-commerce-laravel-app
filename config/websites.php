<?php


 return [

 	'sites' => [ 'HWD' =>'healthwisdom.com' ,
				'DTM'=>'dietministry.com' ,
				'GHP'=>'greghowlett.com',
				'CSS'=>'caringsunshine.com' ,
				'ECS'=>'espanol.caringsunshine.com' ,
				'BDO'=>'bluedotoils.com',
				'RGB'=>'redgatebeauty.com',
				'VCD'=>'viacremedelight.com',
				'EMS'=>'espanol.miraclesunshine.com',
				'MSC'=>'miraclesunshine.com' 
	],

	"woocommerce_order_statuses" => [
				"pending", "processing", "on-hold", "completed", "cancelled", "refunded", "failed"

	],

	"woocommerce_order_statuses_list" => [
				"pending" =>"pending", 
				"processing"=> "processing", 
				"on-hold"=> "on-hold",
				"completed" => "completed", 
				"cancelled" => "cancelled", 
				"refunded" => "refunded",
				"failed" =>  "failed"
	],

	'shipping_method' => ['normal' => 'Normal',
						'priority' => 'Priority',
						'express' => 'Express',
						'2-day air' => '2-day air',
						'overnight' => 'Overnight'
	],

	'woocommerce_api' => [
		"healthwisdom.com" => ["ck_0a2ab06e53a6d2aeafa35c6a76672e99b7ce6e8d","cs_f1087f7d2e00ce901fd588c4341f7d205c1a0c73"],
		"dietministry.com" => ["ck_27ddecb214b85c3a439a6d8e307b2fc2e911b18e","cs_8bafeab22e8768431ed1991f3649a3446f558f56"],
		"bluedotoils.com" => ["ck_37b72f00231df7f83e0d418b3bf097ef1c88c393","cs_8f097d0b45c419c6f2bcf75002079afaf60b2151"],
		"redgatebeauty.com" => ["ck_f097cbe44468190cefa5b738f3ed97f003b9a8fa","cs_4d41c89bbec4a7cffc38ed2dcfe725a19c617fae"],
		"viacremedelight.com" => ["ck_850ef24b4b5b966375d6534d8a7336c50916dd40","cs_2d28527b9827bad446b2c7931df99bf54aac22c3"],
		"espanol.miraclesunshine.com" => ["ck_93c4b0054f3defeb02d1c8971d9304ff9118581d","cs_92951509f433ce2cd45ca5fd68abd0e1c7c0a8f4"],
		"miraclesunshine.com" => ["ck_4d3e926ffacaa430dc2fdb0746e40dffd9671e03","cs_cdc0af02e0537ce125158a2dfe67893aae198771"],
		"greghowlett.com" => ["ck_8a1cffd11e4db92d1b856dc7fc99023f71b97d2a","cs_5f8f4be4bd194baad6c89f1ca0e5381e20bd9225"],
		"caringsunshine.com" => ["ck_112cdb903fd8efdf75a8ee664b68b553e36fdd52","cs_690378d84c998275106bdaa5b293fdd9c93a968d"],
		"espanol.caringsunshine.com" => ["ck_ab183a8e52ae54d73bc78b654403e4e48bd73f4a","cs_38c49c54fca8f7dd2d3e41b2c9cb81e334554cba"],
	],

	'site_shipping_type' => [
		"healthwisdom.com" => "order_total",
		"dietministry.com" => "delivery_location",
		"bluedotoils.com" => "delivery_location",
		"redgatebeauty.com" => "delivery_location",
		"viacremedelight.com" => "delivery_location",
		"espanol.miraclesunshine.com" => "delivery_location",
		"miraclesunshine.com" => "delivery_location",
		"greghowlett.com" => "delivery_location",
		"caringsunshine.com" => "products_weight",
		"espanol.caringsunshine.com" => "products_weight",
	],

	"shipping_type" => ["delivery_location", "order_total","products_weight"],

	'braintree_info' => [

			"caringsunshine.com" => ["descriptor_name" => 'NaturesSunsh*HealthPrd',"descriptor_phone" => '8885107196', "descriptor_url" => '', 'merchant_id' => 'HealthWisdom_instant'],
			"espanol.caringsunshine.com" => ["descriptor_name" => 'NaturesSunsh*HealthPrd',"descriptor_phone" => '8885107196', "descriptor_url" => '', 'merchant_id' => 'HealthWisdom_instant'],
			"miraclesunshine.com" => ["descriptor_name" => 'NaturesSunsh*HealthPrd',"descriptor_phone" => '8885107196', "descriptor_url" => '', 'merchant_id' => 'HealthWisdom_instant'],
			"espanol.miraclesunshine.com" => ["descriptor_name" => 'NaturesSunsh*HealthPrd',"descriptor_phone" => '8885107196', "descriptor_url" => '', 'merchant_id' => 'HealthWisdom_instant'],
			"greghowlett.com" => ["descriptor_name" => 'GregHowlettX*MusicProd',"descriptor_phone" => '8888159082', "descriptor_url" => '', 'merchant_id' => 'GregHowlettProductions_instant'],
			"viacremedelight.com" => ["descriptor_name" => 'ViacremeDelX*OnlinePur',"descriptor_phone" => '8886728460', "descriptor_url" => '', 'merchant_id' => 'ViacremeDelight_instant'],
			"healthwisdom.com" => ["descriptor_name" => 'Healthwisdom*HealthPrd',"descriptor_phone" => '8886728460', "descriptor_url" => '', 'merchant_id' => 'HealthWisdom_instant'],
			"dietministry.com" => ["descriptor_name" => 'Dietministry*HealthPrd',"descriptor_phone" => '8886728460', "descriptor_url" => '', 'merchant_id' => 'DietMinistry_instant'],
			"bluedotoils.com" => ["descriptor_name" => 'doTERRA*EssentialOilsX',"descriptor_phone" => '8885664827', "descriptor_url" => '', 'merchant_id' => 'HealthWisdom_instant'],
			"redgatebeauty.com" => ["descriptor_name" => 'RedGateBeaut*OnlinePur',"descriptor_phone" => '8885664827', "descriptor_url" => '', 'merchant_id' => 'HealthWisdom_instant'],
	]







 ];